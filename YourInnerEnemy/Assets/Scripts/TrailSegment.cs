﻿using System;
using Plugins.TobeCore.Interactions;
using UnityEngine;
using Zenject;

public class TrailSegment : MonoBehaviour
{
    [Inject] private IGameSettingsRepository _gameSettingsRepository;

    private float speed = 0;

    private void Awake()
    {
        _gameSettingsRepository.LoadSettingsAsync(settings =>
        {
            speed = settings.PlayerSpeed;
            print(speed);
        });
    }

    private void Update()
    {
        var rootTransform = transform;

        if (Input.GetKey(KeyCode.KeypadPlus))
        {
            speed += .02f;
            _gameSettingsRepository.SaveSettingsAsync(new GamePlaySettings
            {
                PlayerSpeed = speed
            });
        }

        if (Input.GetKey(KeyCode.KeypadMinus))
        {
            speed -= .02f;
            _gameSettingsRepository.SaveSettingsAsync(new GamePlaySettings
            {
                PlayerSpeed = speed
            });
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            AppHelper.Quit();
        }

        rootTransform.position += speed * -1 * Time.deltaTime * rootTransform.up;
    }
}