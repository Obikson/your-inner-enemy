﻿using System;
using TMPro;
using UnityEngine;

namespace Controllers
{
    public class InGameCanvasController
    {
        private GameSessionController _gameSessionController;

        public bool IsPaused { get; private set; }
        
        public InGameCanvasController(GameSessionController gameSessionController)
        {
            _gameSessionController = gameSessionController;
        }

        public void UpdateScore(TextMeshProUGUI label)
        {
            label.text = _gameSessionController.Score.ToString();
        }

        public void TogglePaused()
        {
            IsPaused = !IsPaused;
            Time.timeScale = IsPaused ? 0 : 1;
        }
    }
}