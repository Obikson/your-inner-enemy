﻿using UnityEngine;

namespace Controllers
{
    public interface IPlayerController
    {
        void EvaluatePlayerTriggerEnter(PlayerControllerProxy playerControllerProxy, GameObject other);
    }
    
}