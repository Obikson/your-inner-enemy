﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Controllers
{
    public class PlayerController : IPlayerController
    {
        private GameSessionController _gameSessionController;


        public PlayerController(GameSessionController gameSessionController)
        {
            _gameSessionController = gameSessionController;
        }

        public void EvaluatePlayerTriggerEnter(PlayerControllerProxy playerControllerProxy, GameObject other)
        {
            if (other.CompareTag("Obstacle"))
            {
                SceneManager.LoadScene("SampleScene");
            }
            else if (other.CompareTag("Entrance"))
            {
                var entranceProxy = other.GetComponent<EntranceControllerProxy>();

                _gameSessionController.Score++;
                
                entranceProxy.passSfx.LoadAssetAsync<AudioClip>().Completed += handle =>
                {
                    playerControllerProxy.audioSource.PlayOneShot(handle.Result);
                };
            }
        }
    }
}