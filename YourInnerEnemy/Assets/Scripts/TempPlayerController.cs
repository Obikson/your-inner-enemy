﻿using System;
using UnityEngine;

public class TempPlayerController : MonoBehaviour
{
    public Transform root;
    private Vector2? lastTouch;
    private void Update()
    {
        var amount =   Time.deltaTime;

        if (Input.touchCount > 0)
        {
         
            amount *= Input.touches[0].deltaPosition.x * 10;
        }
        else
        {
            amount *= 180 *Input.GetAxis("Horizontal");
        }
        
        root.Rotate(root.forward, amount);
    }
}