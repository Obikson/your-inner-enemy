﻿using UnityEngine;


[RequireComponent(typeof(Renderer))]
public class ObstacleSegment : MonoBehaviour
{
    private MaterialPropertyBlock _materialPropertyBlock;
    private int _glowColorParamId;
    private Renderer _renderer;

  
    public void SetGlowColor(Color color)
    {
        _glowColorParamId = Shader.PropertyToID("_Glow_Color");
    
        _renderer = GetComponent<Renderer>();

        _materialPropertyBlock = new MaterialPropertyBlock();
        
        _materialPropertyBlock.SetColor(_glowColorParamId, color);

        _renderer.SetPropertyBlock(_materialPropertyBlock);
    }
}