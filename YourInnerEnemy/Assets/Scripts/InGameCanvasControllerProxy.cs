﻿using Controllers;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

public class InGameCanvasControllerProxy : MonoBehaviour
{
    [Inject] private InGameCanvasController _inGameCanvasController;
    
    [Inject] private IGameSettingsRepository _gameSettingsRepository;
    
    public TextMeshProUGUI scoreLabel;
    
    public TMP_InputField speedVal;
    
    public GameObject settingsPanel;

    private void Update()
    {
        _inGameCanvasController.UpdateScore(scoreLabel);
    }

    public void PauseGame()
    {
        _inGameCanvasController.TogglePaused();
        settingsPanel.SetActive(_inGameCanvasController.IsPaused);
        PopulateSettingsPanel();
    }

    public void PopulateSettingsPanel()
    {
        _gameSettingsRepository.LoadSettingsAsync(setting => { speedVal.text = setting.PlayerSpeed.ToString(); });
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    
    public void SaveSettings()
    {
        _gameSettingsRepository.SaveSettingsAsync(new GamePlaySettings
        {
            PlayerSpeed = float.Parse(speedVal.text)
        }, () =>
        {
            _inGameCanvasController.TogglePaused();
            settingsPanel.SetActive(_inGameCanvasController.IsPaused);
        });
    }
}