﻿using System;

[Serializable]
public class GamePlaySettings
{
    public float PlayerSpeed { get; set; }
}