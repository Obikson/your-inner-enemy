﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class RimSegment : MonoBehaviour
{
    [HideInInspector] public ObstacleSegment[] segments;


    [HideInInspector] public Color glowColor;

    [HideInInspector] public Color[] pallette = new[]
    {
        Color.red, Color.green, Color.blue, Color.yellow, Color.magenta, Color.cyan
    };

    private int _lastPalletteColor;

    private void Start()
    {
        CollectSegments();
        RandomizeColors();
    }

    public void RandomizeColors()
    {
        var range = Random.Range(0, pallette.Length);

        if (range == _lastPalletteColor)
        {
            range = range == 0 ? 1 : 0;
        }

        _lastPalletteColor = range;

        glowColor = pallette[range];
        
        SetSegmentColor(glowColor);
    }

    private void CollectSegments()
    {
        segments = GetComponentsInChildren<ObstacleSegment>();
    }

    public void SetSegmentColor(Color color)
    {
        foreach (var obstacleSegment in segments)
        {
            obstacleSegment.SetGlowColor(color);
        }
    }
}