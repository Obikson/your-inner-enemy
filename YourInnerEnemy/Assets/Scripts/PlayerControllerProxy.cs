﻿using System;
using Controllers;
using Plugins.TobeCore.Interactions;
using UnityEngine;
using Zenject;

public class PlayerControllerProxy : MonoBehaviour
{
    [Inject] private IPlayerController _playerController;

    [SerializeField] private ProximityWatcher proximityWatcher;

    private void Start()
    {
        proximityWatcher.objectEnteredProximity.AddListener(ProximityTrigger);
    }

    private void ProximityTrigger(ProximityWatcher arg0, GameObject o)
    {
        _playerController.EvaluatePlayerTriggerEnter(this, o);
    }

    public AudioSource audioSource;
}
