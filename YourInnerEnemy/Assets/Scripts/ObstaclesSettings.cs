﻿using UnityEngine;


[CreateAssetMenu(fileName = "Obstacles settings", menuName = "Obstacles/Settings")]
public class ObstaclesSettings : ScriptableObject
{
    public int obstacleSegmentsCount = 24;
}