﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Events;

public interface IGameSettingsRepository
{
    void LoadSettingsAsync(UnityAction<GamePlaySettings> successCallback);

    void SaveSettingsAsync(GamePlaySettings settings, UnityAction successCallback = null);
}

public class GameSettingsRepository : IGameSettingsRepository
{
    private readonly string path = "Settings";
    private readonly string filename = "gameplay.bin";

    private string GetPathSafe()
    {
#if UNITY_EDITOR
        var unityPath = Application.dataPath;
#else
        var unityPath = Application.persistentDataPath;
#endif
        var fullPath = Path.Combine(unityPath, path);

        if (!Directory.Exists(fullPath))
        {
            Directory.CreateDirectory(fullPath);
        }

        return Path.Combine(fullPath, filename);
    }

    public void LoadSettingsAsync(UnityAction<GamePlaySettings> successCallback)
    {
        var fullPath = GetPathSafe();

        GamePlaySettings result = null;
        if (File.Exists(fullPath))
        {
            using (var file = File.OpenRead(fullPath))
            {
                var reader = new BinaryFormatter();
                result = reader.Deserialize(file) as GamePlaySettings;
            }
        }

        if (result == null)
        {
            result = new GamePlaySettings
            {
                PlayerSpeed = 2.5f
            };
        }

        successCallback?.Invoke(result);
    }

    public void SaveSettingsAsync(GamePlaySettings settings, UnityAction successCallback = null)
    {
        var fullpath = GetPathSafe();

        if (File.Exists(fullpath))
        {
            File.Delete(fullpath);
        }

        using (var file = File.Open(fullpath, FileMode.OpenOrCreate))
        {
            var serializer = new BinaryFormatter();

            serializer.Serialize(file, settings);
        }

        successCallback?.Invoke();
    }
}