﻿namespace Editor
{
    public abstract class TypedEditor<TModel> : UnityEditor.Editor where TModel : UnityEngine.Object
    {
        protected TModel TargetTyped => target as TModel;
    }
}