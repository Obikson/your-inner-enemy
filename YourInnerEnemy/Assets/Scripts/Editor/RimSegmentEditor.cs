﻿using System;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    public static class ArrayHelpers
    {
        public static TArray[] Expand<TArray>(this TArray[] source, TArray def, int amount = 1)
        {
            var old = source;
            var newArray = new TArray[old.Length + amount];
            Array.Copy(source, newArray, source.Length);
            for (int i = old.Length; i < amount; i++)
            {
                newArray[i] = def;
            }

            return newArray;
        }

        public static TArray[] Slice<TArray>(this TArray[] source, int index)
        {
            var old = source;

            var newArray = new TArray[old.Length - 1];


            for (int i = 0; i < old.Length; i++)
            {
                if (i == index)
                    continue;
                if (i > index)
                {
                    newArray[i-1] = old[i];    
                }
                else
                {
                    newArray[i] = old[i];
                }
            }

            return newArray;
        }
    }

    [CustomEditor(typeof(RimSegment))]
    public class RimSegmentEditor : TypedEditor<RimSegment>
    {
        private Color _selected;

        private void OnEnable()
        {
            _selected = TargetTyped.pallette[0];
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUILayout.Label("Color Management");
            if (GUILayout.Button("+"))
            {
                TargetTyped.pallette = TargetTyped.pallette.Expand(Color.grey, 1);
            }


            for (var index = 0; index < TargetTyped.pallette.Length; index++)
            {
                GUILayout.BeginHorizontal();
                var color = TargetTyped.pallette[index];

                var picked = EditorGUILayout.ColorField(color);

                if (picked != color)
                {
                    TargetTyped.pallette[index] = picked;
                    serializedObject.ApplyModifiedProperties();
                }

                if (GUILayout.Button("Pick"))
                {
                    _selected = color;
                }

                if (GUILayout.Button("-", GUILayout.Width(20)))
                {
                    TargetTyped.pallette = TargetTyped.pallette.Slice(index);
                }

                GUILayout.EndHorizontal();
            }


            TargetTyped.SetSegmentColor(_selected);
        }
    }
}