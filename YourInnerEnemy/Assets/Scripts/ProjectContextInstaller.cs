﻿using Controllers;
using Zenject;

public class ProjectContextInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<IPlayerController>().To<PlayerController>().AsSingle();
        Container.BindInterfacesAndSelfTo<GameSessionController>().AsSingle();
        Container.BindInterfacesAndSelfTo<InGameCanvasController>().AsSingle();
        Container.Bind<IGameSettingsRepository>().To<GameSettingsRepository>().AsSingle();
    }
}