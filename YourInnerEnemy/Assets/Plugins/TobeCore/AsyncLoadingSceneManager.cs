﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Plugins.TobeCore
{
    public class AsyncLoadingSceneManager : MonoBehaviour
    {
        public TextMeshProUGUI loadingmessagelabel;
        
        private void Start()
        {
            StartCoroutine(LoadDelayed());
        }

        private IEnumerator LoadDelayed()
        {
            loadingmessagelabel.text = GlobalSessionScriptableObject.Instance.InterLevelMessage;

            yield return new WaitForSecondsRealtime(2);
            
            var levelToBeloaded = GlobalSessionScriptableObject.Instance.LevelToBeLoaded ?? 1;

            SceneManager.LoadSceneAsync(levelToBeloaded);

        }
    }
}