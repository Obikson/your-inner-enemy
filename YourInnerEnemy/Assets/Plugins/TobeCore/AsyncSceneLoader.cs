﻿using System;
using System.Collections;
using UnityEngine;

namespace Plugins.TobeCore
{
    public class AsyncSceneLoader : MonoBehaviour
    {
        public void LoadSceneAsync(string value)
        {
            ApplicationSessionContext.Instance.LoadScene(value);
        }

        public bool loadOnStart;

        public float loadDelay = 0;

        public string sceneName;
        
        private void Start()
        {
            if (loadOnStart)
            {
                StartCoroutine(LoadDelayCoroutine());
            }
        }

        private IEnumerator LoadDelayCoroutine()
        {
            yield return new WaitForSecondsRealtime(loadDelay);
            
            LoadSceneAsync(sceneName);
        }
    }
}