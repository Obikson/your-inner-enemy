﻿using UnityEngine;

namespace Plugins.TobeCore
{
    public sealed class GlobalSessionScriptableObject : ScriptableObject
    {
        private static GlobalSessionScriptableObject _instance;

        public static GlobalSessionScriptableObject Instance => _instance ? _instance : (_instance = CreateInstance<GlobalSessionScriptableObject>());

        public int? LevelToBeLoaded { get; set; }
        
        public string InterLevelMessage { get; set; }
        public int DestinationExpo { get; set; }
    }
}