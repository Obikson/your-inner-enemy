﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Plugins.TobeCore
{
    public sealed class ApplicationSessionContext : MonoBehaviour
    {
        private static  ApplicationSessionContext _instance;

        public static ApplicationSessionContext Instance => _instance;

        public string loadingSceneName;
        
        public string NextSceneName { get; private set; }

        public void LoadScene(string sceneName)
        {
            NextSceneName = sceneName;
            SceneManager.LoadScene(loadingSceneName);
        }

        
        public string ConsumeSceneLoadRequest()
        {
            var result = NextSceneName;

            NextSceneName = null;
            
            return result;
        }
        
        private void Awake()
        {
            if (_instance)
            {
                if (_instance != this)
                {
                    DestroyImmediate(this);
                    return;
                }
            }
            else
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }
        
        
    }
}