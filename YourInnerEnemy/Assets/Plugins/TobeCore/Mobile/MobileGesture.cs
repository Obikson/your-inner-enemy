﻿using UnityEngine;

namespace Plugins.TobeCore.Mobile
{
    public abstract class MobileGesture
    {
        public abstract bool IsMultiGesture { get; }
        public abstract float TraveledVertical { get; }
        public abstract float TraveledRadius { get; }
        public abstract float TraveledHorizontal { get; }
        public GesturePhase Phase { get; set; }
    }

    public class MultiTouchGesture : MobileGesture
    {
        public MultiTouchGesture()
        {
            ZoomDeadzone = (Screen.width / (float) Screen.height) * 10f;
        }

        public override bool IsMultiGesture => true;

        public override float TraveledVertical => Mathf.Abs(Touches[0].position.x - Touches[1].position.x);

        /// <summary>
        /// Expensive
        /// </summary>
        public override float TraveledRadius => (Touches[0].position - Touches[1].position).magnitude;

        public float LastRadius { get; set; }

        public override float TraveledHorizontal => Mathf.Abs(Touches[0].position.y - Touches[1].position.y);

        public float ImmediateZoomDelta => Mathf.Abs(TraveledRadius - LastRadius);

        public ZoomDirection ZoomDirection
        {
            get
            {
                if (ImmediateZoomDelta > ZoomDeadzone)
                {
                    return TraveledRadius - LastRadius > 0 ? ZoomDirection.In : ZoomDirection.Out;
                }
                else
                {
                    return ZoomDirection.None;
                }
            }
        }

        public float ZoomDeadzone { get; }

        public Touch[] Touches { get; set; } = new Touch[2];
    }

    public enum ZoomDirection
    {
        None = 0,
        In = 1,
        Out = 2
    }


    public enum GesturePhase
    {
        None = 0,
        Began = 1,
        Ongoing = 2,
        Ended = 3
    }

    public enum SwipeHorizontalDirection
    {
        None = 0,
        Left = 1,
        Right = 2
    }

    public enum SwipeVerticalDirection
    {
        None = 0,
        Up = 1,
        Down = 2
    }

    public class SingleTouchGesture : MobileGesture
    {
        public float DeltaVert { get; set; }

        public float DeltaHori { get; set; }

        public override bool IsMultiGesture => false;

        public override float TraveledVertical => Mathf.Abs(BeginPoint.x - CurrentPoint.x);

        public override float TraveledRadius => (BeginPoint - CurrentPoint).magnitude;

        public override float TraveledHorizontal => Mathf.Abs(BeginPoint.y - CurrentPoint.y);

        public SwipeHorizontalDirection HorizontalDirection { get; set; } = SwipeHorizontalDirection.None;
        public SwipeHorizontalDirection ImmediateHorizontalDirection { get; set; } = SwipeHorizontalDirection.None;

        public SwipeVerticalDirection VerticalDirection { get; set; } = SwipeVerticalDirection.None;
        public SwipeVerticalDirection ImmediateVerticalDirection { get; set; } = SwipeVerticalDirection.None;

        public Vector2 BeginPoint { get; set; }

        public Vector2 CurrentPoint { get; set; }
        public Vector2 LastPoint { get; set; }


        public float DeadVertRadius { get; set; }
        public float DeadHoriRadius { get; set; }
        public float DeadImmediateVertRadius { get; set; }
        public float DeadImmediateHoriRadius { get; set; }

        public SingleTouchGesture()
        {
            DeadHoriRadius = Screen.width * .2f;
            DeadVertRadius = Screen.height * .2f;
            DeadImmediateVertRadius = Screen.height * .01f;
            DeadImmediateHoriRadius = Screen.width * .01f;
        }
    }
}