﻿using System;
using Plugins.TobeCore.Interactions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;
using Zenject;

namespace Plugins.TobeCore.Mobile
{
    public interface ICursorRaycaster
    {
        bool Enabled { get; set; }
        void EvaluateRaycast();
    }

    public class CursorRaycaster : ICursorRaycaster, IInitializable
    {
        private Camera _mainCamera;

        private LayerMask layerMask;

        public UnityEvent<Collider> onHit;

        private readonly IMobileCursorProvider _cursor;

        private readonly IRaycastService _raycastService;
        
        public CursorRaycaster(IMobileCursorProvider cursor, IRaycastService raycastService)
        {
            _cursor = cursor;
            _raycastService = raycastService;
            _cursor.Tapped += CursorOnTapped;
            layerMask = LayerMask.NameToLayer("CursorRaycaster");
        }

        private void CursorOnTapped(object sender, EventArgs e)
        {
            EvaluateRaycast();
        }


        public void Initialize()
        {
            _mainCamera = Camera.main;
        }

        public void EvaluateRaycast()
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                Debug.Log("Pointer is over UI element " + EventSystem.current.gameObject);
                return;
            }

            if (!Enabled || !_mainCamera) return;

            var ray = _mainCamera.ScreenPointToRay(Input.touchCount > 0
                ? Input.touches[0].position
                : (Vector2) Input.mousePosition);
            
            Debug.DrawRay(ray.origin, ray.direction, Color.magenta, 1);


            var collider = _raycastService.EvaluateRaycast(ray, layerMask);

            if (collider)
            {
                Debug.Log(collider.transform);
                
                var selectable = collider.GetComponent<Selectable>();
                if (selectable)
                {
                    selectable.ToggleSelect();
                }
                onHit?.Invoke(collider);

            }
            

        }

        public bool Enabled { get; set; } = true;
    }
}