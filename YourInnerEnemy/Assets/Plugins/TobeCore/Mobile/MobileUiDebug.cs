﻿using TMPro;
using UnityEngine;
using Zenject;

namespace Plugins.TobeCore.Mobile
{
    public class GestureServiceTickable : ITickable
    {
        private readonly IMobileGestureService _gestureService;

        public GestureServiceTickable(IMobileGestureService gestureService)
        {
            _gestureService = gestureService;
        }

        public void Tick()
        {
            _gestureService.Update();
        }
    }
    
    public class MobileUiDebug : MonoBehaviour
    {
        [Inject] private IMobileGestureService _gestureService;

        public TextMeshProUGUI label;

        // Start is called before the first frame update
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {

            label.text = string.Empty;

            if (_gestureService.CurrentGesture == null)
            {
                label.text += "No gesture";
            }
            else
            {
                label.text += $"Is multi-gesture: {_gestureService.CurrentGesture.IsMultiGesture}\n";
                label.text += $"Radius: {_gestureService.CurrentGesture.TraveledVertical}\n";

                if (_gestureService.CurrentGesture.IsMultiGesture)
                {
                    var multiGesture = _gestureService.CurrentGesture as MultiTouchGesture;

                    label.text += $"Scale : {multiGesture.ZoomDirection}\n" +
                                  $"d-{multiGesture.ImmediateZoomDelta}\n";
                    
                }
                else
                {
                    var singleGesture = _gestureService.CurrentGesture as SingleTouchGesture;

                    label.text += $"Swipe deadzone: h-{singleGesture.DeadHoriRadius}, v-{singleGesture.DeadVertRadius}\n";
                    label.text += $"Swipe: h-{singleGesture.HorizontalDirection}, v-{singleGesture.VerticalDirection}\n";
                    label.text += $"Touch delta: h-{singleGesture.DeltaHori}, v-{singleGesture.DeltaVert}\n";
                }
            }
        }
    }
}