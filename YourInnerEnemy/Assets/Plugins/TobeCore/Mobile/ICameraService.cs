﻿using UnityEngine;

namespace Plugins.TobeCore.Mobile
{
    public interface ICameraService
    {
        void ThirdPersonTransform(Transform transform, Transform target, float distance, float height,
            float rotationDeg);

        void ThirdPersonWithLineTrack(Transform transform, Transform keepInLine, Transform target, float distance,
            float height);
    }

    public class CameraService : ICameraService
    {
        public void ThirdPersonTransform(Transform transform, Transform target, float distance, float height,
            float rotationDeg)
        {
            var tranfCache = transform;

            tranfCache.rotation = Quaternion.identity;

            var targetPosition = target.position;

            var position = targetPosition;

            position -= tranfCache.forward * distance;

            position += tranfCache.up * height;

            tranfCache.position = position;


            tranfCache.RotateAround(targetPosition, Vector3.up, rotationDeg);

            transform.LookAt(target);
        }

        public void ThirdPersonWithLineTrack(Transform transform, Transform keepInLine, Transform target,
            float distance, float height)
        {
            var tranfCache = transform;

            tranfCache.rotation = Quaternion.identity;

            var targetPosition = target.position;

            var position = targetPosition;

            var distanceKeep = (targetPosition - keepInLine.position);
            position -= targetPosition + distanceKeep.normalized * (distance + distanceKeep.magnitude);

            position += tranfCache.up * height;

            tranfCache.position = position;

            transform.LookAt(keepInLine);
        }
    }
}