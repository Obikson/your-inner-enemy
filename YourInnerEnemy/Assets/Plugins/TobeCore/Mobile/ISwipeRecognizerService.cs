﻿namespace Plugins.TobeCore.Mobile
{
    public interface ISwipeRecognizerService
    {
        void GetSwipeHorizontal(SingleTouchGesture singleTouchGesture);

        void GetSwipeVertical(SingleTouchGesture singleTouchGesture);
    }
}