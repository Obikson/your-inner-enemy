﻿using System;
using UnityEngine;

namespace Plugins.TobeCore.Mobile
{
    public interface IMobileCursorProvider
    {
        bool IsTapped { get; }
        
        Vector3 TapScreenApproxLocation { get; }

        Vector3 TapWorldApproxLocation(float distance);

        event EventHandler Tapped;
        
        Ray TapRay { get; }
        
        Vector3? ScreenTouchVelocity { get; }
        
        void Update();
    }
}