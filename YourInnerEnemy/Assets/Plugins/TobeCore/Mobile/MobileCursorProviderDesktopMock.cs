﻿using System;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

namespace Plugins.TobeCore.Mobile
{
    public class MobileCursorProviderDesktopMock : IMobileCursorProvider, ITickable
    {
        public MouseButton tapSimulated = MouseButton.LeftMouse;

        private bool _lastTap;

        public bool IsTapped => Input.GetMouseButton((int) tapSimulated);

        public Vector3 TapScreenApproxLocation => Input.mousePosition;

        public Vector3 TapWorldApproxLocation(float distance)
        {
            var v3 = TapScreenApproxLocation;
            
            v3.z = distance;

            return Camera.main.ScreenToWorldPoint(v3);
        }

        public event EventHandler Tapped;

        public Ray TapRay => Camera.main.ScreenPointToRay(TapScreenApproxLocation);

        public Vector3? ScreenTouchVelocity { get; private set; }
        
        private Vector3 _lastPosition;
        
        private bool _firstUpdate;
        
        public void Update()
        {
            if (!_firstUpdate)
            {
                _firstUpdate = true;

                ScreenTouchVelocity = _lastPosition - TapScreenApproxLocation;
            }
            
            var value = IsTapped;
                
            if (_lastTap != value && value)
            {
                Tapped?.Invoke(this, EventArgs.Empty);
            }

            _lastTap = value;
            
            _lastPosition = TapScreenApproxLocation;
        }

        public void Tick()
        {
            Update();
        }
    }
}