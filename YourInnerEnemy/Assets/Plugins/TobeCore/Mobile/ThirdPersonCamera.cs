﻿using System;
using UnityEngine;
using Zenject;

namespace Plugins.TobeCore.Mobile
{
    public class ThirdPersonCamera : MonoBehaviour
    {
        public Transform target;

        [Inject] private IMobileGestureService _gestureService;
        [Inject] private ICameraService _cameraService;
        public float minDistance = 5;

        public float maxDistance = 30;


        public float height = 10;
        public float heightIncrease = 10f;
        public float distance = 20;

        public float rotationDeg = 0;

        public bool keepInLineWith;

        public Transform keepInLine;

        public float keepInLineDistance = 20;
        public float keepInLineHeight = 20;

        public float rotationSpeed = 30;

        public float zoomSpeed = 30;

        public float minHeight = 0;

        public float maxHeight = 50;

        public float rotationVelocityDecay = 0.1f;
        public bool increaseVelocity;
        public AnimationCurve curve;
        public float time;

        void Update()
        {
            rotationDeg += curve.Evaluate(time) * (increaseVelocity ? 1 : -1);

            time = Mathf.Clamp01(time - rotationVelocityDecay);

            if (_gestureService.HasGesture)
            {
                if (_gestureService.CurrentGesture.IsMultiGesture)
                {
                    var multi = _gestureService.GetCurrentGesture<MultiTouchGesture>();
                    if (multi.ZoomDirection != ZoomDirection.None)
                    {
                        var zoomAmount = zoomSpeed * (multi.ZoomDirection == ZoomDirection.In ? -1 : 1) *
                                         Time.deltaTime;

                        distance = Mathf.Clamp(zoomAmount + distance, minDistance, maxDistance);
                    }
                }
                else
                {
                    var single = _gestureService.GetCurrentGesture<SingleTouchGesture>();

                    if (single.ImmediateHorizontalDirection != SwipeHorizontalDirection.None)
                    {
                        var rotAmount = rotationSpeed *
                                        (single.ImmediateHorizontalDirection == SwipeHorizontalDirection.Left
                                            ? -1
                                            : 1) * Time.deltaTime;

                        increaseVelocity = single.ImmediateHorizontalDirection == SwipeHorizontalDirection.Right;

                        if (single.DeltaHori > 30)
                        {
                            time = Mathf.Clamp(time + single.DeltaHori * Time.deltaTime, -0, 1);
                        }
                        else
                        {
                            time = 0;
                        }


                        rotationDeg += rotAmount;

                        if (rotationDeg > 360)
                        {
                            rotationDeg -= 360;
                        }
                        else if (rotationDeg < 0)
                        {
                            rotationDeg += 360;
                        }
                    }
                    else if (single.ImmediateVerticalDirection != SwipeVerticalDirection.None)
                    {
                        var heightAmount = heightIncrease *
                                           (single.ImmediateVerticalDirection == SwipeVerticalDirection.Down
                                               ? -1
                                               : 1) * Time.deltaTime;

                        height = Mathf.Clamp(heightAmount + height, minHeight, maxHeight);
                    }
                }
            }

            UpdateTransform();
        }

        public void UpdateTransform()
        {
            if (keepInLineWith)
            {
                _cameraService.ThirdPersonWithLineTrack(transform, keepInLine, target, keepInLineDistance,
                    keepInLineHeight);
            }
            else
            {
                _cameraService.ThirdPersonTransform(transform, target, distance, height, rotationDeg);
            }
        }

        public void KeepInLineToggle(bool value)
        {
            keepInLineWith = value;
        }

        public void KeepInLineToggle()
        {
            keepInLineWith = !keepInLineWith;
        }
    }

    public static class RotateAroundPivotExtensions
    {
        //Returns the rotated Vector3 using a Quaterion
        public static Vector3 RotateAroundPivot(this Vector3 Point, Vector3 Pivot, Quaternion Angle)
        {
            return Angle * (Point - Pivot) + Pivot;
        }

        //Returns the rotated Vector3 using Euler
        public static Vector3 RotateAroundPivot(this Vector3 Point, Vector3 Pivot, Vector3 Euler)
        {
            return RotateAroundPivot(Point, Pivot, Quaternion.Euler(Euler));
        }

        //Rotates the Transform's position using a Quaterion
        public static void RotateAroundPivot(this Transform Me, Vector3 Pivot, Quaternion Angle)
        {
            Me.position = Me.position.RotateAroundPivot(Pivot, Angle);
        }

        //Rotates the Transform's position using Euler
        public static void RotateAroundPivot(this Transform Me, Vector3 Pivot, Vector3 Euler)
        {
            Me.position = Me.position.RotateAroundPivot(Pivot, Quaternion.Euler(Euler));
        }
    }

    /*USAGE:
    *
    * myGameObject.transform.RotateAroundPivot(CenterAsVector3, AngleAsVector3); //Modifies the Transform (immediate rotation)
    * myGameObject.transform.position.RotateAroundPivot(CenterAsVector3, AngleAsVector3); //Returns the rotated position (preview rotation)
    *
    */
}