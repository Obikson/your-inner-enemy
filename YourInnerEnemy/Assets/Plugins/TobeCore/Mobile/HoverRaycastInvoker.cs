﻿using Plugins.TobeCore.Interactions;
using UnityEngine;
using Zenject;

namespace Plugins.TobeCore.Mobile
{
    public class HoverRaycastInvoker : MonoBehaviour
    {
        public LayerMask layerMask;

        public float invokeInterval = .5f;

        public float invokeDelay = 0f;

        [Inject] public IRaycastService _raycastService;

        [SerializeField] private RaycastProvider _raycastProvider;

        private void Start()
        {
            InvokeRepeating(nameof(InvokeLogic), invokeDelay, invokeInterval);
        }

        private void OnDisable()
        {
            CancelInvoke(nameof(InvokeLogic));
        }

        private void OnEnable()
        {
            InvokeRepeating(nameof(InvokeLogic), invokeDelay, invokeInterval);
        }

        private void OnDestroy()
        {
            CancelInvoke(nameof(InvokeLogic));
        }

        private void InvokeLogic()
        {
            var ray = _raycastProvider.GetRay();
            
            var otherCollider = _raycastService.EvaluateRaycast(ray, layerMask)?.GetComponent<Hoverable>();
            print(otherCollider);
            if (otherCollider)
            {
                if (lastHovered && lastHovered == otherCollider)
                {
                    return;
                }

                if (lastHovered && lastHovered != otherCollider)
                {
                    lastHovered.InvokeOnHoverExit();
                }
                
                otherCollider.InvokeOnHoverEnter();
            }
            else
            {
                if (lastHovered)
                {
                    lastHovered.InvokeOnHoverExit();
                }
            }

            lastHovered = otherCollider;
        }

        public Hoverable lastHovered;
    }
}