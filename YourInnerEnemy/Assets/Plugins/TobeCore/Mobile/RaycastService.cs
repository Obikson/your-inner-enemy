﻿using System;
using Plugins.TobeCore.Interactions;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Plugins.TobeCore.Mobile
{
    public interface IRaycastService
    {
        Collider EvaluateRaycast(Ray ray, int layerMask);
    }

    public class RaycastService : IRaycastService
    {
        private RaycastHit _hit;

        public Collider EvaluateRaycast(Ray ray, int layerMask)
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                Debug.Log("Pointer is over UI element " + EventSystem.current.gameObject);
                return null;
            }

            if (!Enabled) return null;


            Debug.DrawRay(ray.origin, ray.direction, Color.magenta, 1);

            if (Physics.Raycast(ray, out _hit, 2000, 1 << layerMask))
            {
                Debug.Log(_hit.transform);
                
                return _hit.collider;
            }

            return null;
        }

        public bool Enabled { get; set; } = true;
    }
}