﻿using UnityEngine;

namespace Plugins.TobeCore.Mobile
{
    public abstract class RaycastProvider : MonoBehaviour
    {
        public abstract Ray GetRay();
    }
}