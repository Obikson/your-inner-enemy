﻿using UnityEngine;
using Zenject;

namespace Plugins.TobeCore.Mobile
{
    public class GestureRotator : MonoBehaviour
    {
        [Inject] private IMobileGestureService _gestureService;

        [Tooltip("Default == this")] public Transform root;

        public float speed = 0.1f;

        void Start()
        {
            if (!root)
                root = transform;
        }

        void Update()
        {
            if (!_gestureService.HasGesture || _gestureService.CurrentGesture.IsMultiGesture)
            {
                return;
            }

            var singleTouchGesture = _gestureService.GetCurrentGesture<SingleTouchGesture>();


            root.rotation *= Quaternion.AngleAxis(
                speed * singleTouchGesture.DeltaHori *
                (singleTouchGesture.ImmediateHorizontalDirection == SwipeHorizontalDirection.Right ? -1 : 1), root.up);
        }
    }
}