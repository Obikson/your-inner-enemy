﻿using UnityEngine;

namespace Plugins.TobeCore.Mobile
{
    public class SwipeRecognizerService : ISwipeRecognizerService
    {
        public void GetSwipeHorizontal(SingleTouchGesture singleTouchGesture)
        {
            if (Mathf.Abs(singleTouchGesture.CurrentPoint.x - singleTouchGesture.BeginPoint.x) >=
                singleTouchGesture.DeadHoriRadius)
            {
                singleTouchGesture.HorizontalDirection =
                    singleTouchGesture.CurrentPoint.x > singleTouchGesture.BeginPoint.x
                        ? SwipeHorizontalDirection.Right
                        : SwipeHorizontalDirection.Left;
            }
            else
            {
                singleTouchGesture.HorizontalDirection = SwipeHorizontalDirection.None;
            }
            
            if (Mathf.Abs(singleTouchGesture.CurrentPoint.x - singleTouchGesture.LastPoint.x) >=
                singleTouchGesture.DeadImmediateHoriRadius)
            {
                singleTouchGesture.ImmediateHorizontalDirection=
                    singleTouchGesture.CurrentPoint.x > singleTouchGesture.LastPoint.x
                        ? SwipeHorizontalDirection.Right
                        : SwipeHorizontalDirection.Left;
            }
            else
            {
                singleTouchGesture.ImmediateHorizontalDirection = SwipeHorizontalDirection.None;
            }
        }

        public void GetSwipeVertical(SingleTouchGesture singleTouchGesture)
        {
            if (Mathf.Abs(singleTouchGesture.CurrentPoint.y - singleTouchGesture.BeginPoint.y) >=
                singleTouchGesture.DeadVertRadius)
            {
                singleTouchGesture.VerticalDirection =
                    singleTouchGesture.CurrentPoint.y < singleTouchGesture.BeginPoint.y
                        ? SwipeVerticalDirection.Down
                        : SwipeVerticalDirection.Up;
            }
            else
            {
                singleTouchGesture.VerticalDirection = SwipeVerticalDirection.None;
            }
            
            if (Mathf.Abs(singleTouchGesture.CurrentPoint.y - singleTouchGesture.LastPoint.y) >=
                singleTouchGesture.DeadImmediateVertRadius)
            {
                singleTouchGesture.ImmediateVerticalDirection =
                    singleTouchGesture.CurrentPoint.y < singleTouchGesture.LastPoint.y
                        ? SwipeVerticalDirection.Down
                        : SwipeVerticalDirection.Up;
            }
            else
            {
                singleTouchGesture.ImmediateVerticalDirection = SwipeVerticalDirection.None;
            }
            
        }
    }
}