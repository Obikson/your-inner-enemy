﻿using UnityEngine;

namespace Plugins.TobeCore.Mobile
{
    public  class MobileGestureService : IMobileGestureService
    {
        public MobileGesture CurrentGesture { get; private set; }
        public TGesture GetCurrentGesture<TGesture>() where TGesture : MobileGesture
        {
            return CurrentGesture as TGesture;
        }

        public bool HasGesture => CurrentGesture != null;

        private readonly MultiTouchGesture _multiTouchGesture = new MultiTouchGesture();
        private readonly SingleTouchGesture _singleTouchGesture = new SingleTouchGesture();
        private readonly ISwipeRecognizerService _swipeRecognizerService = new SwipeRecognizerService();

        public  void Update()
        {
            if (Input.touchCount > 0)
            {
                if (Input.touchCount > 1)
                {
                    CurrentGesture = _multiTouchGesture;

                    if (CurrentGesture != _multiTouchGesture)
                    {
                        _multiTouchGesture.Phase = GesturePhase.Began;
                    }
                    _multiTouchGesture.LastRadius = _multiTouchGesture.TraveledRadius;

                    _multiTouchGesture.Touches[0] = Input.touches[0];

                    _multiTouchGesture.Touches[1] = Input.touches[1];
                }
                else
                {
                    if (CurrentGesture != _singleTouchGesture)
                    {
                        _singleTouchGesture.Phase = GesturePhase.Began;

                        _singleTouchGesture.BeginPoint = Input.touches[0].position;
                        _singleTouchGesture.CurrentPoint = Input.touches[0].position;
                        _singleTouchGesture.LastPoint = _singleTouchGesture.CurrentPoint;
                    }

                    _singleTouchGesture.DeltaHori =
                        Mathf.Abs(_singleTouchGesture.CurrentPoint.x - Input.touches[0].position.x);
                    
                    _singleTouchGesture.DeltaVert =
                        Mathf.Abs(_singleTouchGesture.CurrentPoint.y - Input.touches[0].position.y);

                    _singleTouchGesture.LastPoint = _singleTouchGesture.CurrentPoint;

                    
                    _singleTouchGesture.CurrentPoint = Input.touches[0].position;
                    
                    _swipeRecognizerService.GetSwipeHorizontal(_singleTouchGesture);
                    
                    _swipeRecognizerService.GetSwipeVertical(_singleTouchGesture);
                    
                    CurrentGesture = _singleTouchGesture;
                }
            }
            else
            {
                CurrentGesture = null;
            }
        }
    }
}