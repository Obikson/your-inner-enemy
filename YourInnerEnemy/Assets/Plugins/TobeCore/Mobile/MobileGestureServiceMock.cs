﻿using UnityEngine;
using UnityEngine.UIElements;

namespace Plugins.TobeCore.Mobile
{
    public class MobileGestureServiceMock : IMobileGestureService
    {
        public MobileGesture CurrentGesture { get; private set; }
        public TGesture GetCurrentGesture<TGesture>() where TGesture : MobileGesture
        {
            return CurrentGesture as TGesture;
        }

        public bool HasGesture => CurrentGesture != null;

        private readonly MultiTouchGesture _multiTouchGesture = new MultiTouchGesture();
        private readonly SingleTouchGesture _singleTouchGesture = new SingleTouchGesture();
        private readonly ISwipeRecognizerService _swipeRecognizerService = new SwipeRecognizerService();

        public void Update()
        {
            if (Input.GetMouseButton((int) MouseButton.RightMouse))
            {
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    CurrentGesture = _multiTouchGesture;

                    if (CurrentGesture != _multiTouchGesture)
                    {
                        _multiTouchGesture.Phase = GesturePhase.Began;
                    }

                    _multiTouchGesture.LastRadius = _multiTouchGesture.TraveledRadius;
                    
                    _multiTouchGesture.Touches[0] = new Touch {position = _singleTouchGesture.BeginPoint};
                    
                    _multiTouchGesture.Touches[1] = new Touch {position = Input.mousePosition};
                                        
                }
                else
                {
                    if (CurrentGesture != _singleTouchGesture)
                    {
                        _singleTouchGesture.Phase = GesturePhase.Began;

                        _singleTouchGesture.BeginPoint = Input.mousePosition;
                        _singleTouchGesture.LastPoint = _singleTouchGesture.CurrentPoint;
                        _singleTouchGesture.CurrentPoint = Input.mousePosition;
                    }
                    
                    _singleTouchGesture.DeltaHori =
                        Mathf.Abs(_singleTouchGesture.CurrentPoint.x - Input.mousePosition.x);
                    
                    _singleTouchGesture.DeltaVert =
                        Mathf.Abs(_singleTouchGesture.CurrentPoint.y - Input.mousePosition.y);

                    _singleTouchGesture.LastPoint = _singleTouchGesture.CurrentPoint;
                    
                    _singleTouchGesture.CurrentPoint = Input.mousePosition;

                    _swipeRecognizerService.GetSwipeHorizontal(_singleTouchGesture);
                    
                    _swipeRecognizerService.GetSwipeVertical(_singleTouchGesture);

                    CurrentGesture = _singleTouchGesture;
                }
            }
            else
            {
                CurrentGesture = null;
            }
        }
    }
}