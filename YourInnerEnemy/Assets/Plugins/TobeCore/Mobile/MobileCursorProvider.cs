﻿using System;
using UnityEngine;
using Zenject;

namespace Plugins.TobeCore.Mobile
{
    public class MobileCursorProvider : IMobileCursorProvider, ITickable
    {
        public bool IsTapped => Input.touchCount > 0;

        public Vector3 TapScreenApproxLocation => Input.touchCount == 0 ? Vector2.zero : Input.touches[0].position;

        //Todo:
        public Vector3 TapWorldApproxLocation(float distance)
        {
            var v3 = TapScreenApproxLocation;

            v3.z = distance;

            return Camera.main.ScreenToWorldPoint(v3);
        }

        public event EventHandler Tapped;
        private Vector3 _lastPosition;

        private bool _firstUpdate;

        //Todo:
        public Ray TapRay => Camera.main.ScreenPointToRay(TapScreenApproxLocation);

        public Vector3? ScreenTouchVelocity { get; private set; }
        private bool _lastTap;

        public void Update()
        {
            if (!_firstUpdate)
            {
                _firstUpdate = true;

                ScreenTouchVelocity = _lastPosition - TapScreenApproxLocation;
            }

            var value = IsTapped;

            if (_lastTap != value && value)
            {
                Tapped?.Invoke(this, EventArgs.Empty);
            }

            _lastTap = value;

            _lastPosition = TapScreenApproxLocation;
        }

        public void Tick()
        {
            Update();
        }
    }
}