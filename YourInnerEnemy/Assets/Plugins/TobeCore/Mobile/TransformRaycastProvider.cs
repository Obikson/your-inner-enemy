﻿using System;
using UnityEngine;

namespace Plugins.TobeCore.Mobile
{
    [RequireComponent(typeof(Transform))]
    public class TransformRaycastProvider : RaycastProvider
    {
        private Ray _rayCache;

        public override Ray GetRay()
        {
            var t = transform;
            
            _rayCache.origin = t.position;

            _rayCache.direction = t.forward;
            
            return _rayCache;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;

            Gizmos.DrawRay(GetRay());

        }
    }
}