﻿namespace Plugins.TobeCore.Mobile
{
    public interface IMobileGestureService
    {
        MobileGesture CurrentGesture { get; }
        
        TGesture GetCurrentGesture<TGesture>() where TGesture : MobileGesture;
        
        bool HasGesture { get; }

        void Update();
    }
}