﻿using System;
using UnityEngine;
using Zenject;

namespace Plugins.TobeCore.Mobile
{
    public class GestureOrbiter : MonoBehaviour
    {
        [Tooltip("Pivot")] public Transform pivot;

        [Inject] private IMobileGestureService _gestureService;

        public float speed = 10f;

        public Vector3 initialOffsetPosition;
        private float _rotationAmount;

        private void Start()
        {
            if (initialOffsetPosition == Vector3.zero)
            {
                initialOffsetPosition = transform.position;
            }
        }

        void Update()
        {
            if (_gestureService.HasGesture && !_gestureService.CurrentGesture.IsMultiGesture)
            {
                var singleTouchGesture = _gestureService.GetCurrentGesture<SingleTouchGesture>();


                if (singleTouchGesture.ImmediateHorizontalDirection != SwipeHorizontalDirection.None)
                {
                    _rotationAmount =
                        (singleTouchGesture.HorizontalDirection == SwipeHorizontalDirection.Left ? 1 : -1) * speed *
                        singleTouchGesture.DeltaHori * Time.deltaTime;
                }
                else
                {
                    _rotationAmount = 0;
                }
            }

            transform.position = RotatePointAroundPivot(pivot.position + initialOffsetPosition, pivot.position,
                new Vector3(0, _rotationAmount, 0));
            transform.LookAt(pivot);
        }

        private Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles)
        {
            var dir = point - pivot; // get point direction relative to pivot
            dir = Quaternion.Euler(angles) * dir; // rotate it
            point = dir + pivot; // calculate rotated point
            return point; // return it
        }
    }
}