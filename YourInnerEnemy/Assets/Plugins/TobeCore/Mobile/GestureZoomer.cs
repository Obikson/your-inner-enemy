﻿using System;
using UnityEngine;
using Zenject;

namespace Plugins.TobeCore.Mobile
{
    public class GestureZoomer : MonoBehaviour
    {
        [Inject] private IMobileGestureService _gestureService;

        [Tooltip("Zooms current object toward another object")]
        public Transform Toward;

        public float speed = 10f;
        
        private void Start()
        {
            Align();
        }

        public void Align()
        {
            transform.LookAt(Toward);
        }

        private void Update()
        {
            if (!_gestureService.HasGesture || !_gestureService.CurrentGesture.IsMultiGesture)
            {
                return;
            }

            var multiG = _gestureService.GetCurrentGesture<MultiTouchGesture>();

            if (multiG.ZoomDirection != ZoomDirection.None)
            {
                Align();
                transform.position += (multiG.ZoomDirection == ZoomDirection.In ? 1f : -1f) * speed * Time.deltaTime * transform.forward;
            }
        }
    }
}