﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Plugins.TobeCore
{
    public class AsyncSceneConsumer : MonoBehaviour
    {
        private AsyncOperation _asyncOperation;

        public float delayInSecs;
        
        private void Start()
        {
            var sceneToLoad = ApplicationSessionContext.Instance.ConsumeSceneLoadRequest();

            if (string.IsNullOrEmpty(sceneToLoad))
            {
                throw new ArgumentNullException("No scene to be loaded");
            }

            _asyncOperation = SceneManager.LoadSceneAsync(sceneToLoad, LoadSceneMode.Single);

            StartCoroutine(LoadingCoroutine());
        }

        private IEnumerator LoadingCoroutine()
        {
            yield return new WaitForSecondsRealtime(delayInSecs);
            yield return _asyncOperation;
        }
    }
}