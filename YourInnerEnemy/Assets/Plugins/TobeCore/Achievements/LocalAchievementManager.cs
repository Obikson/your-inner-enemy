﻿using UnityEngine;

namespace Plugins.TobeCore.Achievements
{
    public class LocalAchievementManager : AchievementManager
    {
        private const string AchievementsKey = "achievements";
        
        protected override void SaveUserAchievements(AchievementSerializedData achievements)
        {
            var value = JsonUtility.ToJson(achievements);
            PlayerPrefs.SetString(AchievementsKey, value);
            PlayerPrefs.Save();
        }

        protected override AchievementSerializedData LoadUserAchievements()
        {
            var data = PlayerPrefs.GetString(AchievementsKey);
            
            if(string.IsNullOrEmpty(data))
                return new AchievementSerializedData
                {
                    data = new AchievementSerializedDataModel[0]
                };
            
            var value = JsonUtility.FromJson<AchievementSerializedData>(data);

            return value;
        }
    }
}