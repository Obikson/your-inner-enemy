﻿using UnityEngine;

namespace Plugins.TobeCore.Achievements
{
    
    public abstract class Achievement : ScriptableObject
    {
        public int id;
        public string displayTitle;
        public string description;
        public bool isUnique = true;
        public int triggerAfterAttempts = 1;
    }
}