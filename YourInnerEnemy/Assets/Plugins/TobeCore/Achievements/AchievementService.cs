﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Plugins.TobeCore.Achievements
{
    public class AchievementService : IAchievementService
    {
        private IAchievementRepository _achievementRepository;

        public event EventHandler<Achievement> AchievementAssigned;

        private AchievementManager _achievementManager;

        public AchievementService(IAchievementRepository achievementRepository)
        {
            _achievementRepository = achievementRepository;
            _achievementManager = new LocalAchievementManager();
        }

        private readonly Dictionary<int, int> _achievementAttempts = new Dictionary<int, int>();

        public bool TryAssignAchievement(int id)
        {
            var achievement = _achievementRepository.GetRegisteredAchievements().FirstOrDefault(x => x.id == id);

            if (!achievement)
            {
                Debug.LogError("Achievement with id " + id + " was not found in repository.");
                return false;
            }

            if (_achievementManager.HasUserAchievement(id))
                return false;

            if (!_achievementAttempts.ContainsKey(id))
            {
                _achievementAttempts.Add(id, 0);
            }

            _achievementAttempts[id]++;

            if (_achievementAttempts[id] < achievement.triggerAfterAttempts)
            {
                Debug.Log(
                    $"Not enough trigger attempts - {_achievementAttempts[id]} / {achievement.triggerAfterAttempts}");
                return false;
            }

            Debug.Log($"Achievement assigned {achievement.id} {achievement.displayTitle}");

            _achievementManager.GiveUserAchievement(id);

            OnAchievementAssigned(achievement);

            return true;
        }


        public void ClearAll()
        {
            _achievementManager.ClearAchievements();
        }

        public IEnumerable<Achievement> GetUserAchievements()
        {
            var userAchievement = _achievementManager.GetCurrentAchievements();
            var result = new List<Achievement>();

            foreach (var registeredAchievement in _achievementRepository.GetRegisteredAchievements())
            {
                if (userAchievement.Any(x => x.achievementId == registeredAchievement.id))
                    result.Add(registeredAchievement);
            }

            return result;
        }

        public IEnumerable<Achievement> GetAllAchievements()
        {
            return _achievementRepository.GetRegisteredAchievements();
        }

        protected virtual void OnAchievementAssigned(Achievement e)
        {
            AchievementAssigned?.Invoke(this, e);
        }
    }
}