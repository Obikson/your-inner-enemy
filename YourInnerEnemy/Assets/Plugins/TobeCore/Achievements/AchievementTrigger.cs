﻿using UnityEngine;
using Zenject;

namespace Plugins.TobeCore.Achievements
{
    public class AchievementTrigger : MonoBehaviour
    {
        [Inject] private IAchievementService _achievementService;

        public Achievement achievement;

        public void TriggerAchievement()
        {
            if (!achievement) return;


            if (_achievementService.TryAssignAchievement(achievement.id))
                print("Achievement awarded");
        }
    }
}