﻿using System;
using System.Collections.Generic;

namespace Plugins.TobeCore.Achievements
{
    public interface IAchievementService
    {
        bool TryAssignAchievement(int id);
        event EventHandler<Achievement> AchievementAssigned;
        void ClearAll();
        IEnumerable<Achievement> GetUserAchievements();
        IEnumerable<Achievement> GetAllAchievements();
    }
}