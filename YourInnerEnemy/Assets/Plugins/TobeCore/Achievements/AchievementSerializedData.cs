﻿using System;

namespace Plugins.TobeCore.Achievements
{
    [Serializable]
    public class AchievementSerializedData
    {
        public AchievementSerializedDataModel[] data;
    }
}