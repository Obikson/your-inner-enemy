﻿using System;

namespace Plugins.TobeCore.Achievements
{
    [Serializable]
    public class AchievementSerializedDataModel
    {
        public int achievementId;
        public long achievedUtcTicks;
    }
}