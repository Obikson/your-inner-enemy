﻿using System;
using System.Linq;

namespace Plugins.TobeCore.Achievements
{
    public abstract class AchievementManager
    {
        protected abstract void SaveUserAchievements(AchievementSerializedData achievements);

        protected abstract AchievementSerializedData LoadUserAchievements();

        public void GiveUserAchievement(int id)
        {
            var achievements = LoadUserAchievements();
            
            if (achievements.data.Any(x => x.achievementId == id))
                return;

            var data = achievements.data.ToList();
            
            data.Add(new AchievementSerializedDataModel
            {
                achievementId = id,
                achievedUtcTicks = DateTime.UtcNow.Ticks
            });
            
            SaveUserAchievements(new AchievementSerializedData
            {
                data = data.ToArray()
            });
        }

        public void ClearAchievements()
        {
            SaveUserAchievements(new AchievementSerializedData {data = new AchievementSerializedDataModel[0]});
        }

        public bool HasUserAchievement(int id)
        {
            var achievements = LoadUserAchievements();

            return achievements.data.Any(x => x.achievementId == id);
        }

        public AchievementSerializedDataModel[] GetCurrentAchievements()
        {
            return LoadUserAchievements().data;
        }

    }
}