﻿using UnityEngine;

namespace Plugins.TobeCore.Achievements
{
    public interface IAchievementRepository
    {
        Achievement[] GetRegisteredAchievements();
    }
    
    [CreateAssetMenu(fileName = "Achievement Repository", menuName = "Achievements/Repository")]
    public class AchievementRepository : ScriptableObject, IAchievementRepository
    {
        public Achievement[] achievements;
        
        public Achievement[] GetRegisteredAchievements()
        {
            return achievements;
        }
    }
}