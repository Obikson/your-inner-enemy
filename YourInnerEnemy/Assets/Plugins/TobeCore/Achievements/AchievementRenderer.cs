﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

namespace Plugins.TobeCore.Achievements
{
    public class AchievementRenderer : MonoBehaviour
    {
        public CanvasGroup canvasGroup;

        public TextMeshProUGUI label;

        //TODO: tohle je jen pro DPO
        public Image logo;

        public UnityEvent onAchievementAssigned;
        
        [Inject]
        private IAchievementService _achievementService;

        private void Start()
        {
            _achievementService.AchievementAssigned += AchievementServiceOnAchievementAssigned;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                _achievementService.ClearAll();
            }
        }

        private void AchievementServiceOnAchievementAssigned(object sender, Achievement e)
        {
            onAchievementAssigned?.Invoke();
            label.text = e.displayTitle;
            StopAllCoroutines();
            StartCoroutine(ShowCoroutine());
        }

        private void OnDestroy()
        {
            StopAllCoroutines();
        }

        private IEnumerator ShowCoroutine()
        {
            logo.enabled = false;
            canvasGroup.alpha = 1;
            yield return new WaitForSecondsRealtime(4);
            canvasGroup.alpha = 0;
            logo.enabled = true;

        }

        public void LoadAchievementScene()
        {
            SceneManager.LoadScene("Scenes/AchievementsScene");
        }
    }
}