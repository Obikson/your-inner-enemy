﻿using UnityEngine;
using Zenject;

namespace Plugins.TobeCore
{
    public abstract class UiManager : MonoBehaviour
    {
        [Inject] private IGlobalSession _globalSession;
        
        protected abstract int AsyncSceneIndex { get; set; }

        public void LoadSceneAsync(int sceneIndex, string loadingMessage)
        {
            _globalSession.ChangeScene(sceneIndex, AsyncSceneIndex, loadingMessage);
        }

        public abstract void LoadSceneAsync(int sceneIndex);
    }
}

