﻿using Plugins.TobeCore.Utils;
using UnityEngine;
using Zenject;

namespace Plugins.TobeCore.Dialogs
{
    public class ChangeDialogTrigger : TriggerBehavior
    {
        [Inject]
        public DialogManager manager;

        public Dialog dataModel;
        
        protected override void OnTriggered()
        {
            manager.BindRoot(dataModel);
            manager.SequenceStart();
        }
    }
}