﻿using Plugins.TobeCore.UI;

namespace Plugins.TobeCore.Dialogs
{
    public class DialogRenderer : ListRenderer<Dialog, MdButton>
    {
        public DialogManager manager;

        public MdText dialogLabel;

        private int _created;

        public void RenderSequence()
        {
            var currentSequence = manager.CurrentItem;

            if (currentSequence == null) return;

            dialogLabel.SetText(currentSequence.GetNpcDialog());

            Clear();

            if (!currentSequence.HasOptions)
            {
                return;
            }

            _created = currentSequence.options.Length;

            Bind(currentSequence.options, (index, dialog, button) =>
            {
                button.Clicked.AddListener(() =>
                {
                    manager.MoveNext(dialog.Id);
                });

                button.SetText(dialog.optionText);

                button.indexText.text = optionAlphaIndexes[index];

                button.mdAnimation.SlideIn();

            });
        }

        private int _next;
    }
}