﻿using System;
using UnityEngine;

namespace Plugins.TobeCore.Dialogs
{
    [Serializable]
    public class DialogCharacterCommand
    {
        public DialogCharacter character;

        public string playAnimationState;

        public bool setActive = true;
        
        public string executeMethod;
        
        [Tooltip("In seconds")]
        public float executionDelay;
    }
}