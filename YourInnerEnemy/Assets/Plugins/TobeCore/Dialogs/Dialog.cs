﻿using System.Linq;
using Plugins.TobeCore.Achievements;
using Plugins.TobeCore.Utils;
using UnityEngine;

namespace Plugins.TobeCore.Dialogs
{
    
    [CreateAssetMenu(fileName = "Dialog File", menuName = "Dialogs/Dialog File - Single Value")]
    public class Dialog : ScriptableObject, IOptionListItem<Dialog>
    {
        [SerializeField, Tooltip("Unique identifier")]
        private int id;

        [Tooltip("This will be shown as option text ex. A,B,C")]
        public string optionText;
        
        [Tooltip("This will be shown on top ex.: \"Choose your option\"")]
        public string npcDialog;

        public virtual string GetNpcDialog()
        {
            return npcDialog;
        }
        
        public Dialog[] options;

        public DialogCharacterCommand[] commands;

        

        public Achievement triggerAchievement;
        
        public int Id
        {
            get => id;
            set => id = value;
        }

        public Dialog[] Options
        {
            get => options;
            set => options = value;
        }

        public bool HasOptions => Options.Length > 0 && Options.All(x => x != null);
    }
}