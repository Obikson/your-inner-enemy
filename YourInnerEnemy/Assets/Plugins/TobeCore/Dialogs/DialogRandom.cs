﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Plugins.TobeCore.Dialogs
{
    [CreateAssetMenu(fileName = "Dialog File - Random Value", menuName = "Dialogs/Dialog File - Random Value")]
    public class DialogRandom : Dialog
    {
        public RandomDialogValue[] values = new[]
        {
            new RandomDialogValue
            {
                probability = 25,
                npcDialog = "Value no. 1 - prob = 25"
            },
            new RandomDialogValue
            {
                probability = 25,
                npcDialog = "Value no. 2 - prob = 25"
            },
            new RandomDialogValue
            {
                probability = 25,
                npcDialog = "Value no. 3 - prob = 25"
            },
            new RandomDialogValue
            {
                probability = 25,
                npcDialog = "Value no. 4 - prob = 25"
            },
        };

        public override string GetNpcDialog()
        {
            var random = Random.Range(0, values.Length);

            return values[random].npcDialog;
        }
    }
    
    

    [Serializable]
    public class RandomDialogValue
    {
        public string npcDialog;

        [Tooltip("Not yet implemented")]
        public float probability;
    }
}