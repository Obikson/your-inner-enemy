﻿using System.Linq;
using Plugins.TobeCore.Achievements;
using Plugins.TobeCore.Utils;
using Zenject;

namespace Plugins.TobeCore.Dialogs
{
    public class DialogManager : LinkedListContainerBehavior<Dialog>, IDialogManager
    {
        public Dialog dialogFile;

        public DialogCharacter[] charactersInDialog;

        [Inject] private IAchievementService _achievementService;

        protected override void Start()
        {
            base.Start();

            if (dialogFile)
            {
                BindRoot(dialogFile);
                SequenceStart();
            }
        }

        protected override void OnItemChanged()
        {
            base.OnItemChanged();

            if (CurrentItem != null)
            {
                if (CurrentItem.triggerAchievement)
                {
                    _achievementService.TryAssignAchievement(CurrentItem.triggerAchievement.id);
                }

                var commands = CurrentItem.commands;

                if (commands != null)
                {
                    print("Executing commands - len " + commands.Length);

                    foreach (var command in commands)
                    {
                        print("- Command - " + command.playAnimationState);
                        
                        var charactersToCommand = charactersInDialog.Where(x => x.id == command.character.id);

                        foreach (var character in charactersToCommand)
                        {
                            print("- - Applying - " + character.id);

                            character.assetRenderer.DataModel.IsActive.Value = command.setActive;

                            if (!string.IsNullOrEmpty(command.playAnimationState))
                            {
                                character.assetRenderer.PlayState(command.playAnimationState);
                            }

                            if (!string.IsNullOrEmpty(command.executeMethod))
                            {
                                character.Invoke(command.executeMethod, command.executionDelay);
                            }
                        }
                    }
                }
            }
        }

        public void AddCharacterToDialog(DialogCharacter character)
        {
            if (charactersInDialog.Any(x => x.id == character.id)) return;

            charactersInDialog = charactersInDialog.Append(character).ToArray();
        }
    }
}