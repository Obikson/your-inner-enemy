﻿using System;
using Plugins.TobeCore.Mvc;
using UnityEngine;
using Zenject;

namespace Plugins.TobeCore.Dialogs
{
    public class DialogCharacter : MonoBehaviour
    {
        [Inject] private DialogManager _dialogManager;
        
        public AssetRendererBase assetRenderer;

        public string id = Guid.NewGuid().ToString();
        
        protected void Start()
        {
            if (_dialogManager)
            {
                _dialogManager.AddCharacterToDialog(this);
            }
        }

    }
}