﻿using Plugins.TobeCore.UI.Editor;
using UnityEditor;
using UnityEngine;

namespace Plugins.TobeCore.Dialogs.Editor
{
    [CustomEditor(typeof(ChangeDialogTrigger))]
    public class DialogTriggerEditor : EditorType<ChangeDialogTrigger>
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Trigger"))
            {
                TargetTyped.Trigger();
            }
        }
    }
    [CustomEditor(typeof(Dialog))]
    public class DialogEditor : EditorType<Dialog>
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
        }
    }
    
    
}