﻿using System;

namespace Plugins.TobeCore.Dialogs
{
    [Serializable]
    public class DialogOption
    {
        public string text;

        public Dialog leadsTo;
    }
}