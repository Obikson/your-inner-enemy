﻿using System;
using Plugins.TobeCore.Mobile;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

namespace Plugins.TobeCore
{
    public class IdleService : ITickable
    {
        private IMobileCursorProvider _mobileCursorProvider;

        public IdleService(IMobileCursorProvider mobileCursorProvider)
        {
            _mobileCursorProvider = mobileCursorProvider;
            _mobileCursorProvider.Tapped += MobileCursorProviderOnTapped;
        }

        private void MobileCursorProviderOnTapped(object sender, EventArgs e)
        {
            Reset();
            
        }

        public bool IsIdle { get; private set; }

        public void Reset()
        {
            _lastTouched = Time.timeSinceLevelLoad;
            IsIdle = false;
        }
        
        public bool Enabled { get; set; } = true;

        public event EventHandler Idle;

        private float _lastTouched;
        
        private float idleInterval = 4;

        public void Tick()
        {
            if (_lastTouched + idleInterval <= Time.timeSinceLevelLoad)
            {
                if (!IsIdle && Enabled)
                {
                    OnIdle();
                }

                IsIdle = true;
            }
            else
            {
                IsIdle = false;
            }
        }

        protected virtual void OnIdle()
        {
            Idle?.Invoke(this, EventArgs.Empty);
        }
    }
}

