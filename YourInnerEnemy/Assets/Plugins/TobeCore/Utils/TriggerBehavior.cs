﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace Plugins.TobeCore.Utils
{
    public abstract class TriggerBehavior : MonoBehaviour
    {
        private bool _triggered;
        
        public bool Triggered => _triggered;

        public bool triggersOnlyOnce = true;

        protected abstract void OnTriggered();
        
        public virtual void Trigger()
        {
            if (triggersOnlyOnce)
            {
                if (!Triggered)
                {
                    _triggered = true;
                    OnTriggered();
                }
            }
            else
            {
                _triggered = true;
                OnTriggered();
            }
        }
    }

    [Serializable]
    public class TriggerEventHandler<TDataModel> : UnityEvent<TDataModel> where TDataModel : Object
    {
        public TDataModel dataModel;
    }


    public abstract class TriggerBehavior<TDataModel> : TriggerBehavior where TDataModel : Object
    {
        public TDataModel dataModel;

        public TriggerEventHandler<TDataModel> onTriggered;


        public override void Trigger()
        {
            base.Trigger();
            
            onTriggered?.Invoke(dataModel);
        }
    }
}