﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace Plugins.TobeCore.Utils
{
    public interface ISequenceContainer
    {
    }

    public interface IOptionListItem<TDataModel> where TDataModel : class
    {
        int Id { get; set; }
        TDataModel[] Options { get; set; }
    }

    [Serializable]
    public class SequenceContainerItemChanged : UnityEvent
    {
    }

    [Serializable]
    public class SequenceContainerEventHandler : UnityEvent
    {
    }

    [Serializable]
    public class LinkedListContainerBehaviorEventHandler : UnityEvent
    {
    }

    public abstract class LinkedListContainerBehavior<TDataModel> : MonoBehaviour
        where TDataModel : class, IOptionListItem<TDataModel>
    {
        public bool HasCompleted { get; protected set; }

        private TDataModel _root;

        private TDataModel _currentItem;

        private bool _hasCompleted;


        public TDataModel CurrentItem => _currentItem;


        public LinkedListContainerBehaviorEventHandler hasCompleted;

        public LinkedListContainerBehaviorEventHandler hasStarted;

        public LinkedListContainerBehaviorEventHandler itemChanged;


        public void MoveNext(int id)
        {
            if (CurrentItem != null)
            {
                var next = CurrentItem.Options.FirstOrDefault(x => x.Id == id);

                if (next == null)
                {
                    Debug.LogError("No option with id " + id + " found");
                    return;
                }

                _currentItem = next;

                HasCompleted = CurrentItem.Options.Length == 0 || CurrentItem.Options.All(x => x == null);

                if (HasCompleted)
                {
                    OnHasCompleted();
                }

                OnItemChanged();
            }
        }

        private void OnHasCompleted()
        {
            hasCompleted?.Invoke();
        }

        public void BindRoot(TDataModel root)
        {
            _root = root;
        }

        public void ResetSequence()
        {
            _currentItem = _root;
        }

        public void SequenceStart()
        {
            ResetSequence();
            OnHasStarted();
            OnItemChanged();
        }

        private void OnHasStarted()
        {
            hasStarted?.Invoke();
        }

        protected virtual void OnItemChanged()
        {
            itemChanged?.Invoke();
        }


        protected virtual void Start()
        {
        }

        protected virtual void Update()
        {
        }
    }

    public abstract class SequenceContainerBehaviour<TDataModel> : MonoBehaviour, ISequenceContainer
    {
        private TDataModel[] _data;

        private int _currentIndex = 0;

        public SequenceContainerItemChanged CurrentItemChanged;

        public SequenceContainerEventHandler SequenceComplete;
        public SequenceContainerEventHandler SequenceStarted;

        private bool _hasFinished;

        public void BindData(TDataModel[] data)
        {
            _data = data;
        }

        protected abstract void ItemChanged(TDataModel dataModel);

        public void StartSequence()
        {
            Restart();
            MoveNext();
            OnSequenceStarted();
        }

        private void OnSequenceStarted()
        {
            SequenceStarted?.Invoke();
        }

        public void MoveNext()
        {
            if (!_hasFinished && NextItem != null)
            {
                _currentIndex++;
                ItemChanged(CurrentItem);
                OnCurrentItemChanged();
            }
            else
            {
                if (!_hasFinished)
                {
                    OnSequenceComplete();
                }

                _hasFinished = true;
            }
        }

        private void OnSequenceComplete()
        {
            SequenceComplete?.Invoke();
        }

        private void OnCurrentItemChanged()
        {
            CurrentItemChanged?.Invoke();
        }

        public void Restart()
        {
            _currentIndex = -1;
        }

        protected TDataModel CurrentItem
        {
            get
            {
                if (_data == null || _data.Length == 0 || _currentIndex < 0 || _currentIndex >= _data.Length)
                    return default;

                return _data[_currentIndex];
            }
        }

        protected TDataModel NextItem
        {
            get
            {
                if (_data == null || _data.Length == 0 || _currentIndex + 1 < 0 || _currentIndex + 1 >= _data.Length)
                    return default;

                return _data[_currentIndex + 1];
            }
        }

        protected TDataModel PrevItem
        {
            get
            {
                if (_data == null || _data.Length == 0 || _currentIndex - 1 < 0 || _currentIndex - 1 >= _data.Length)
                    return default;

                return _data[_currentIndex - 1];
            }
        }

        protected virtual void Update()
        {
        }

        protected virtual void Start()
        {
        }
    }
}