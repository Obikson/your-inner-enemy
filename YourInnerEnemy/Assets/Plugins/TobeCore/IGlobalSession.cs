﻿namespace Plugins.TobeCore
{
    public interface IGlobalSession
    {
        int ExpositionDestinationId { get; set; }

        void ChangeScene(int sceneIndex, int asyncSceneIndex, string loadingMessage = null);
    }
}