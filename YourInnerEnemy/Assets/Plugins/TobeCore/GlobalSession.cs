﻿using System;
using UnityEngine.SceneManagement;
using Zenject;

namespace Plugins.TobeCore
{
    public class GlobalSession : IGlobalSession
    {
        public int ExpositionDestinationId
        {
            get => GlobalSessionScriptableObject.Instance.DestinationExpo;
            set => GlobalSessionScriptableObject.Instance.DestinationExpo = value;
            
        }

        public void ChangeScene(int sceneIndex, int asyncSceneIndex, string loadingMessage = null)
        {
            GlobalSessionScriptableObject.Instance.LevelToBeLoaded = sceneIndex;
            GlobalSessionScriptableObject.Instance.InterLevelMessage = loadingMessage;
            SceneManager.LoadScene(asyncSceneIndex);
        }
    }
}