﻿using Plugins.TobeCore.Achievements;
using Zenject;

namespace Plugins.TobeCore.Installers
{
    public class AchievementInstallers : MonoInstaller
    {
        public AchievementRepository achievementRepository;
        
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<AchievementService>().AsSingle();
            Container.BindInterfacesAndSelfTo<AchievementRepository>().FromScriptableObject(achievementRepository).AsSingle();
            Container.BindInterfacesAndSelfTo<LocalAchievementManager>().AsSingle();
        }
    }
}