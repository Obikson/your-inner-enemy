﻿using Plugins.TobeCore.Mobile;
using Zenject;

namespace Plugins.TobeCore.Installers
{
    public class MobileInputInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
#if UNITY_EDITOR
            Container.BindInterfacesAndSelfTo<MobileCursorProviderDesktopMock>().AsSingle();
#else
                        Container.BindInterfacesAndSelfTo<MobileCursorProvider>().AsSingle();
#endif
            
            Container.BindInterfacesAndSelfTo<CursorRaycaster>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<RaycastService>().AsSingle().NonLazy();
        }
    }
}