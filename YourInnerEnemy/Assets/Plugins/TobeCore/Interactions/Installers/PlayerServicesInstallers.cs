﻿using Zenject;

namespace Plugins.TobeCore.Installers
{
    public class PlayerServicesInstallers : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<IdleService>().AsSingle().NonLazy();
        }
    }
}