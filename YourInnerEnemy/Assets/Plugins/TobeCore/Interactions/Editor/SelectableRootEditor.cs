﻿using Plugins.TobeCore.UI.Editor;
using UnityEditor;
using UnityEngine;

namespace Plugins.TobeCore.Interactions.Editor
{
    [CustomEditor(typeof(SelectableRoot))]
    public class SelectableRootEditor : EditorType<SelectableRoot>
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Select"))
            {
                TargetTyped.Select();
            }
        }
    }
}
