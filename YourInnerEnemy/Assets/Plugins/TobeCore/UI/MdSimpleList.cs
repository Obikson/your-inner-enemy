﻿namespace Plugins.TobeCore.UI
{
    public class MdSimpleList : MdRepeater<string>
    {
        
        protected override void Start()
        {
            onBind.AddListener((index, item) =>
            {
                var text = items[index];
                item.GetComponentInChildren<MdText>().SetText(text);
            });
            base.Start();
        }
    }
}