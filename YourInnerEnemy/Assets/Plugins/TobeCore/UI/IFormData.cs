﻿namespace Plugins.TobeCore.UI
{
    public interface IFormData
    {
        string Name { get; set; }
        string Value { get; set; }
    }
}