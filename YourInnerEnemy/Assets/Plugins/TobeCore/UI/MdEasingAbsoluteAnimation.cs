﻿using System;
using UnityEngine;

namespace Plugins.TobeCore.UI
{
    public class MdEasingAbsoluteAnimation : MonoBehaviour
    {
        public AnimationCurve smoothing = AnimationCurve.EaseInOut(0, 0, 1, 1);

        public float progress;

        public float speed = 1;


        private RectTransform _rectTransform;


        private void Start()
        {
            _rectTransform = GetComponent<RectTransform>();

        }

        [SerializeField]
        private bool _isVisible;
        
        [SerializeField]
        private bool _completed ;

        private void Update()
        {
            if (_completed) return;

            progress = Mathf.Clamp01(progress + speed * Time.deltaTime);

            if (progress >= 1)
            {
                _completed = true;
            }
            
            var xVal = 0f;
            
            if (!_isVisible)
            {
                xVal = _rectTransform.rect.height * (1 - smoothing.Evaluate(progress));
            }
            else
            {
                xVal = _rectTransform.rect.height * smoothing.Evaluate(progress);
            }

            
            _rectTransform.anchoredPosition = new Vector2(0, progress * xVal);
        }

        public void ToggleEase()
        {
            _completed = false;
            progress = 0;
            _isVisible = !_isVisible;
        }

        public void EaseIn()
        {
            _isVisible = true;
            progress = 0;
            _completed = false;
        }
    }
}