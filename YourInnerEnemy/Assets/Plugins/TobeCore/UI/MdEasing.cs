﻿namespace Plugins.TobeCore.UI
{
    public enum SlideDirection
    {
        None = 0,
        Out = 1,
        In = 2
    }
}