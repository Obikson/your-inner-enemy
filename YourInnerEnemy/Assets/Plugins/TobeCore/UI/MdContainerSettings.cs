﻿using System;
using UnityEngine;

namespace Plugins.TobeCore.UI
{
    [Serializable]
    public class MdContainerSettings 
    {
        public Vector4 containerPadding = new Vector4(15, 15, 15, 15);
        public Color containerBackgroundColor = Color.black;
        public Sprite containerBackgroundImage;
    }
}