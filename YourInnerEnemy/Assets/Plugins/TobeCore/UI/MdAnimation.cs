﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Plugins.TobeCore.UI
{
    public class MdAnimation : MonoBehaviour
    {
        [SerializeField] private Animator _animator;

        [SerializeField] private string _slideInStateName = "slide in";

        [SerializeField] private string _slideOutStateName = "slide out";

        [SerializeField] private string _fadeInStateName = "fade in";

        [SerializeField] private string _fadeOutStateName = "fade out";

        private MdAnimationStateBehavior _animationStateBehavior;

        private void Start()
        {
            if (_animator)
            {
                _animator = GetComponent<Animator>();
                _animationStateBehavior = _animator.GetBehaviour<MdAnimationStateBehavior>();
            }

            if (!destroyOnSlideOut)
            {
                destroyOnSlideOut = gameObject;
            }
        }

        public void SlideIn()
        {
            _currentAnimation = MdAnimationType.SlideIn;
            _animator.Play(_slideInStateName);
        }

        public void SlideOut()
        {
            _currentAnimation = MdAnimationType.SlideOut;
            _animator.Play(_slideOutStateName);
        }

        public void FadeIn()
        {
            _currentAnimation = MdAnimationType.FadeIn;
            _animator.Play(_fadeInStateName);
        }

        public void FadeOut()
        {
            _currentAnimation = MdAnimationType.FadeOUt;
            _animator.Play(_fadeOutStateName);
        }

        public bool autoDestroyOnSlideOut;
        public GameObject destroyOnSlideOut;
        private MdAnimationType _currentAnimation;

        public void OnAnimationComplete(MdAnimationType type)
        {
            onAnimationComplete?.Invoke(type, this);

            if (type == MdAnimationType.SlideOut)
            {
                Destroy(destroyOnSlideOut);
            }
        }

        public MdAnimationEventHandler onAnimationComplete;
    }

    [Serializable]
    public class MdAnimationEventHandler : UnityEvent<MdAnimationType, MdAnimation>
    {
    }

    public enum MdAnimationType
    {
        None,
        SlideOut,
        FadeIn,
        FadeOUt,
        SlideIn
    }
}