﻿using UnityEngine;
using UnityEngine.UI;

namespace Plugins.TobeCore.UI
{
    public class MdContainer : MdElement
    {
        public Image background;
        public RectTransform content;

        private Canvas _canvas;

        protected override void OnEnable()
        {
            base.OnEnable();
            _canvas = FindObjectOfType<Canvas>();
        }

        public override void Refresh()
        {
            base.Refresh();

            content.StretchMax();

            content.offsetMin = new Vector2(
                MdSettings.Settings.containerSettings.containerPadding.x,
                MdSettings.Settings.containerSettings.containerPadding.y
            );        

            content.offsetMax = new Vector2(
                -MdSettings.Settings.containerSettings.containerPadding.z,
                -MdSettings.Settings.containerSettings.containerPadding.w
            );


            background.color = MdSettings.Settings.containerSettings.containerBackgroundColor;

            background.sprite = MdSettings.Settings.containerSettings.containerBackgroundImage;
        }
    }
}