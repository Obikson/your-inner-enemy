﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Plugins.TobeCore.UI
{
    [Serializable]
    public class MdIconSet 
    {
        public IconSetPair[] icons;

        
    }

    public static class MdIconSetHelpers
    {
        public static Sprite  GetIcon(this MdIconSet set, string key)
        {
            return set.icons.FirstOrDefault(x => x.key.Equals(key))?.icon;
        }
    }
}