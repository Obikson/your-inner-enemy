﻿using System;
using UnityEngine;

namespace Plugins.TobeCore.UI
{
    [Serializable]
    public class MdCheckboxSettings 
    {
        public Sprite checkboxChecked;
        public Sprite checkboxEmpty;
        public Vector4 checkboxLabelMargin = new Vector4(15, 15, 15, 15);
    }
}