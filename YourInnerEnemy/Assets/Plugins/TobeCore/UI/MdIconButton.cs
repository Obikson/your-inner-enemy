﻿using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Plugins.TobeCore.UI
{
    [RequireComponent(typeof(Button))]
    public class MdIconButton : MdElement, IMdButton
    {
        public Image graphics;

        public Button button;

        public UnityEvent Clicked => button.onClick;

        public string icon = "menu";

        public override void Refresh()
        {
            var _parentElement = GetParentContrast();
                
            graphics.sprite = MdSettings.Settings.iconSet.GetIcon(icon);
            
            graphics.color = _parentElement != null && _parentElement.IsDark
                ? MdSettings.Settings.fontSettings.fontColorLight
                : MdSettings.Settings.fontSettings.fontColorDark;
        }
    }
}