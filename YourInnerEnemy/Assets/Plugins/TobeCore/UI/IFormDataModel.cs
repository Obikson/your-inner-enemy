﻿namespace Plugins.TobeCore.UI
{
    public interface IFormDataModel
    {
        IFormData[] Data { get; set; }
    }
}