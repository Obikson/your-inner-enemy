﻿using UnityEngine;
using UnityEngine.UI;

namespace Plugins.TobeCore.UI
{
    [RequireComponent(typeof(Image))]    
    public class MdTopBar : MdElement, IMdTopBar
    {
        public Image image;

        public MdSideBar sideBar;
        
        public void ToggleMenu(bool? value = null)
        {
            if (!value.HasValue)
                value = !sideBar.IsVisible;
            
            sideBar.IsVisible = value.Value;
        }

        public override void Refresh()
        {
            base.Refresh();
            isDark = true;
            var rectTransform = GetComponent<RectTransform>();

            rectTransform.anchorMin = new Vector2(0, 1);

            rectTransform.anchorMax = new Vector2(1, 1);
            
            rectTransform.pivot = Vector2.up;
            
            rectTransform.anchoredPosition = Vector2.zero;
            
            rectTransform.sizeDelta = new Vector2(0, MdSettings.Settings.topBarSettings.topBarHeight);
            
            image.color = MdSettings.Settings.currentPalette.secondaryColor;

        }
    }
}