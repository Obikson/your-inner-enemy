﻿using System;
using TMPro;

namespace Plugins.TobeCore.UI
{
    public enum MdTextSize
    {
        Normal,
        Small,
        Large,
        Custom
    }

    public class MdText : MdElement, IMdText
    {
        public TextMeshProUGUI tmpLabel;

        public MdTextSize size;

        public override void Refresh()
        {
            base.Refresh();


            tmpLabel.font = MdSettings.Settings.fontSettings.fontNormal;
            if (GetParentContrast() != null)
            {
                tmpLabel.color = ParentIsDark
                    ? MdSettings.Settings.fontSettings.fontColorLight
                    : MdSettings.Settings.fontSettings.fontColorDark;
            }

            tmpLabel.fontSize = size.EnumToSize();
        }

        public void SetText(string displayText)
        {
            tmpLabel.text = System.Text.RegularExpressions.Regex.Unescape(displayText);
        }
    }

    public static class MdTextHelpers
    {
        public static float EnumToSize(this MdTextSize size, float? custom = null)
        {
            switch (size)
            {
                case MdTextSize.Normal:
                    return MdSettings.Settings.fontSettings.normalSize;
                case MdTextSize.Small:
                    return MdSettings.Settings.fontSettings.smallSize;
                case MdTextSize.Large:
                    return MdSettings.Settings.fontSettings.largeSize;
                case MdTextSize.Custom:
                    return custom ?? MdTextSize.Normal.EnumToSize();
                default:
                    throw new ArgumentOutOfRangeException(nameof(size), size, null);
            }
        }
    }
}