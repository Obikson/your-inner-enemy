﻿namespace Plugins.TobeCore.UI
{
    public interface IMdFormField
    {
        bool IsValid { get; }

        string Value { get; set; }
        string Name { get; set; }

        TValue GetValue<TValue>();
    }
}