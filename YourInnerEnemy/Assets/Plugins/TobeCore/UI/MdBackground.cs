﻿using UnityEngine.UI;

namespace Plugins.TobeCore.UI
{
    public class MdBackground : MdElement
    {
        public Image graphics;

        public override void Refresh()
        {
            base.Refresh();

            if (graphics)
            {
                graphics.color = MdSettings.Settings.containerSettings.containerBackgroundColor;
            }
        }
    }
}