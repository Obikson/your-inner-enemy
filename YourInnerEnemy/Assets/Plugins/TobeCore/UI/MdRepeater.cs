﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Plugins.TobeCore.UI
{
    [Serializable]
    public class RepeaterBindEvent : UnityEvent<int, GameObject>
    {
    }

    public class MdRepeater<TDataModel> : MdElement where TDataModel : class
    {
        public RepeaterBindEvent onBind;

        public GameObject itemPrefab;

        public TDataModel[] items;

        public MdText noitemsText;

        public Transform container;

        protected virtual void Start()
        {
            Bind();
        }

        public void Bind()
        {
            if (!itemPrefab)
                throw new ArgumentNullException("Set item prefab to " + this + " to render items");

            if (!container)
                throw new ArgumentNullException("Set container to " + this + " to render items");

            container.transform.DestroyAllChildren();

            if (items == null || items.Length == 0)
            {
                Instantiate(noitemsText, container);
                return;
            }

            for (var index = 0; index < items.Length; index++)
            {
                var item = Instantiate(itemPrefab, container);
                onBind?.Invoke(index, item);
            }
        }
    }
}