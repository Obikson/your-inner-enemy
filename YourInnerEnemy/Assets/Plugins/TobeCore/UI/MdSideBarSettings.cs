﻿using System;
using UnityEngine;

namespace Plugins.TobeCore.UI
{
    [Serializable]
    public class MdSideBarSettings 
    {
        public float sideBarHeaderHeight = 400f;
    }
}