﻿using System;
using UnityEngine;

namespace Plugins.TobeCore.UI
{
    [Serializable]
    public class MdPaletteScriptableObject
    {
        public Color primaryColor = new Color(66 / 255f, 165 / 255f, 245 / 255f);

        public Color secondaryColor = new Color(1 / 255f, 87 / 255f, 155 / 255f);
        public Color accend = new Color(1 / 255f, 87 / 255f, 155 / 255f);
    }
}