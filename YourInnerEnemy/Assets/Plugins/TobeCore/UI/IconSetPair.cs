﻿using System;
using UnityEngine;

namespace Plugins.TobeCore.UI
{
    [Serializable]
    public class IconSetPair
    {
        public string key;
        public Sprite icon;
    }
}