﻿using TMPro;
using UnityEngine;

namespace Plugins.TobeCore.UI
{
    [CreateAssetMenu(fileName = "Md Settings", menuName = "Material Design/Settings")]
    public class MdSettings : ScriptableObject
    {
        private static MdSettings _settings;

        public static MdSettings Settings =>
            _settings ? _settings : _settings = Resources.Load<MdSettings>("Md Settings");

        public MdPaletteScriptableObject currentPalette;
        public MdIconSet iconSet;
        public MdFontSettings fontSettings;
        public MdTopBarSettings topBarSettings;
        public MdButtonSettings buttonSettings;
        public MdInputSettings inputSettings;
        public MdCheckboxSettings checkboxSettings;
        public MdContainerSettings containerSettings;
        public MdSideBarSettings sideBarSettings;
        public MdFabSettings fab;
    }
}