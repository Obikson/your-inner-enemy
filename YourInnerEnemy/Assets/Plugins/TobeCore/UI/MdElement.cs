﻿using System;
using System.Linq;
using UnityEngine;

namespace Plugins.TobeCore.UI
{
    public abstract class MdElement : MonoBehaviour, IMdInitializable, IMdColorContrast
    {
        public bool isDark;

        private IMdColorContrast _parentContrast;

        protected bool ParentIsDark => GetParentContrast() != null && GetParentContrast().IsDark;


        public MdAnimation mdAnimation;
        
        protected IMdColorContrast GetParentContrast()
        {
            if (_parentContrast == null)
            {
                _parentContrast = transform.parent.GetComponents<MonoBehaviour>()?.OfType<IMdColorContrast>()
                    .FirstOrDefault();
            }

            return _parentContrast;
        }

        protected virtual void OnEnable()
        {
            Refresh();
        }

        public virtual void Refresh()
        {
        }

        public bool IsDark => isDark;


     

      
    }
    
    
}