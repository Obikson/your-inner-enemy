﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Plugins.TobeCore.UI
{
    public class MdSideBar : MdElement, IMdMenu
    {
        private bool _isVisible;

        public Image image;

        public AnimationCurve easeCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);

        public float progress = 0;

        public float speed = 1;

        public UnityEvent StateChanged => onStateChanged;

        public UnityEvent onStateChanged;

        public RectTransform itemsContainer;
        public RectTransform headerContainer;
        
        private RectTransform _rectTransform;

        private RectTransform _canvasRectTransform;
        
        private bool _completed = true;

        private void Start()
        {
            _rectTransform = GetComponent<RectTransform>();
            _canvasRectTransform = FindObjectOfType<Canvas>().GetComponent<RectTransform>();
        }

        public bool IsVisible
        {
            get => _isVisible;
            set { _isVisible = value; }
        }


        private void Update()
        {
            if (_completed) return;

            progress = Mathf.Clamp01(progress + speed * Time.deltaTime);

            if (progress >= 1)
            {
                _completed = true;
                onStateChanged?.Invoke();
            }

            var xVal = 0f;
            if (!_isVisible)
            {
                xVal = _canvasRectTransform.rect.width * (1 - easeCurve.Evaluate(progress));
            }
            else
            {
                xVal = _canvasRectTransform.rect.width * easeCurve.Evaluate(progress);
            }

            _rectTransform.anchoredPosition = new Vector2(xVal, _rectTransform.anchoredPosition.y);
        }

        public void ToggleVisible()
        {
            if (progress >= 1)
            {
                progress = 0;
            } else if (_isVisible)
            {

                progress = 1 - progress;
                
            }
            
            _completed = false;
            
            IsVisible = !IsVisible;
        }

        public override void Refresh()
        {
            base.Refresh();

            var rectTransform = GetComponent<RectTransform>();

            rectTransform.anchorMin = new Vector2(0, 0);

            rectTransform.anchorMax = new Vector2(0, 1);

            rectTransform.pivot = new Vector2(1, 0);

            var canvas = FindObjectOfType<Canvas>().GetComponent<RectTransform>();

            
            rectTransform.sizeDelta = new Vector2(canvas.rect.width, 0);
            
            rectTransform.anchoredPosition = new Vector2(0, 0);

            
            headerContainer.anchorMin = Vector2.up;
            headerContainer.anchorMax = new Vector2(.8f, 1);
            headerContainer.sizeDelta = new Vector2(0, MdSettings.Settings.sideBarSettings.sideBarHeaderHeight);
            
            itemsContainer.anchorMin = Vector2.up;
            itemsContainer.anchorMax = new Vector2(.8f, 1);
            itemsContainer.sizeDelta = new Vector2(0,   canvas.rect.height - MdSettings.Settings.sideBarSettings.sideBarHeaderHeight);
        }
    }
}