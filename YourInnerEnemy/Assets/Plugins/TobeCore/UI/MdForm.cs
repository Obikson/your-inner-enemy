﻿using System;
using System.Linq;
using UnityEngine;

namespace Plugins.TobeCore.UI
{
    public abstract class MdForm<TDataModel> : MdElement where TDataModel : IFormDataModel
    {
        public IMdFormField[] fields;

        protected override void OnEnable()
        {
            fields = transform.GetComponentsInChildren<MdElement>().OfType<IMdFormField>().ToArray();

            base.OnEnable();
        }

        public bool isFormValid;

        public TDataModel dataModel;

        public void Submit()
        {
            ValidateForm();
            if (!isFormValid)
            {
                return;
            }
        }

        public void PopulateFieldsFromDataModel()
        {
            foreach (var nameValue in dataModel.Data)
            {
                var field = fields.FirstOrDefault(x => x.Name.Equals(nameValue.Name));
                if (field != null)
                {
                    field.Value = nameValue.Value;
                }
            }
        }

        public void GatherDataToModel()
        {
            foreach (var formField in fields)
            {
                var data = dataModel.Data.FirstOrDefault(x => x.Name.Equals(formField.Name));
                if (data != null)
                {
                    data.Value = formField.Value;
                }
            }
        }


        public void ValidateForm()
        {
            isFormValid = true;

            foreach (var formField in fields)
            {
                if (!formField.IsValid)
                {
                    isFormValid = false;
                }
            }
        }
    }
}