﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Plugins.TobeCore.UI
{
    public class MdButton : MdElement, IMdLabel, IMdButton
    {
        public TextMeshProUGUI label;

        public Image graphics;

        public Button button;

        public TextMeshProUGUI indexText;
        
        public override void Refresh()
        {
            base.Refresh();
            var bg = MdSettings.Settings.buttonSettings.lgButtonBackground;
            if (bg)
            {
                graphics.sprite = bg;
            }

            label.fontStyle = MdSettings.Settings.buttonSettings.fontStyle;
            label.fontSize = MdSettings.Settings.buttonSettings.fontSize;

            if (!graphics.sprite)
            {
                graphics.color = MdSettings.Settings.currentPalette.primaryColor;
            }

            label.color = isDark
                ? MdSettings.Settings.fontSettings.fontColorLight
                : MdSettings.Settings.fontSettings.fontColorDark;
        }

        public void SetText(string value)
        {
            label.text = value;
        }

        public string GetText()
        {
            return label.text;
        }


        public UnityEvent Clicked => button.onClick;
    }
}