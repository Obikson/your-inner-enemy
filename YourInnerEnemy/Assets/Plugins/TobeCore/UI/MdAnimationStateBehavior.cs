﻿using UnityEngine;
using UnityEngine.Animations;

namespace Plugins.TobeCore.UI
{
    public class MdAnimationStateBehavior : StateMachineBehaviour
    {
        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
        {
            base.OnStateExit(animator, stateInfo, layerIndex, controller);
        }
    }
}