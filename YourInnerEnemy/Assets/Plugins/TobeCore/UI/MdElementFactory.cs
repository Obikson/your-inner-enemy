﻿using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Plugins.TobeCore.UI
{
    

    public static class RectTransformHelpers
    {
        public static void CenterAnchored(this Transform transform)
        {
            var rectTransform = transform as RectTransform;
rectTransform.anchoredPosition = Vector2.zero;

        }
        
        public static void StretchMax(this Transform transform)
        {
            var rectTransform = transform as RectTransform;

            rectTransform.anchorMin = new Vector2(0, 0);
            rectTransform.anchorMax = new Vector2(1, 1);
            rectTransform.sizeDelta = Vector2.zero;
            ;
        }
    }
}