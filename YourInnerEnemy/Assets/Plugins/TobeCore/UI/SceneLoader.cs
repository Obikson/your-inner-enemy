﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace Plugins.TobeCore.UI
{
    public class SceneLoader : MonoBehaviour
    {
        public int asyncSceneIndex;

        public void LoadSceneAsync(int sceneIndex)
        {
            SceneManager.LoadScene(sceneIndex);
        }

    }
}