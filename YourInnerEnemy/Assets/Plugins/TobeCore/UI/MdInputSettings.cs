﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Plugins.TobeCore.UI
{
    [Serializable]
    public class MdInputSettings 
    {
        public Sprite bgInput;
        public Color placeholderFontColor = new Color(1, 1, 1, .3f);
        public Vector4 inputTextMargin = new Vector4(15, 0, 15, 0);
        public ColorBlock inputColors = ColorBlock.defaultColorBlock;
        public Color inputUnderlineColor = Color.black;
    }
}