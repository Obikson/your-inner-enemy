﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Plugins.TobeCore.UI
{
    public class MdInput : MdElement, IMdInput, IMdFormField
    {
        public TMP_InputField tmpInput;
        public TextMeshProUGUI tmpPlaceholder;
        public TextMeshProUGUI tmpText;
        public Image underline;
        public override void Refresh()
        {
            base.Refresh();
            tmpInput.colors = MdSettings.Settings.inputSettings.inputColors;
            tmpText.color = MdSettings.Settings.fontSettings.fontColorDark;
            tmpPlaceholder.color = MdSettings.Settings.inputSettings.placeholderFontColor;
            tmpPlaceholder.margin = tmpText.margin = MdSettings.Settings.inputSettings.inputTextMargin;
            tmpText.alignment = tmpPlaceholder.alignment = TextAlignmentOptions.BottomLeft;
            underline.color = MdSettings.Settings.inputSettings.inputUnderlineColor;
        }

        public string Text
        {
            get => tmpInput.text;
            set => tmpInput.text = value;
        }
        public string Placeholder
        {
            get => tmpPlaceholder.text;
            set => tmpPlaceholder.text = value;
        }

        public bool IsValid { get; } = true;
        
        public string Value
        {
            get => Text;
            set => Text = value;
        }
        public string Name { get; set; }
        
        public TValue GetValue<TValue>()
        {
            throw new System.NotImplementedException();
        }
    }
}