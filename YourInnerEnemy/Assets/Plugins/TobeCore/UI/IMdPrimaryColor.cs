﻿using UnityEngine;
using UnityEngine.Events;

namespace Plugins.TobeCore.UI
{

    public interface IMdInput : IMdInitializable
    {
        string Text { get; set; }
        string Placeholder { get; set; }
    }
    
    public interface IMdMenu : IMdInitializable
    {
        bool IsVisible { get; set; }
        
        UnityEvent StateChanged { get; }
    }
    
    public interface IMdTopBar: IMdInitializable
    {
        void ToggleMenu(bool? value = null);
    }
   
    public interface IMdButton: IMdInitializable
    {
        UnityEvent Clicked { get; }
    }

    public interface IMdLabel 
    {
        void SetText(string value);

        string GetText();
    }

    public interface IMdInitializable
    {
        void Refresh();
    }

    public interface IMdCheckbox
    {
        string Text { get; set; }
    }

    public interface IMdColorContrast
    {
        bool IsDark { get; }
    }
}    