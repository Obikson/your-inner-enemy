﻿using System;
using TMPro;
using UnityEngine;

namespace Plugins.TobeCore.UI
{
    [Serializable]
    public class MdButtonSettings 
    {
        public string defaultText = "MD Button";
        public Sprite lgButtonBackground;
        public FontStyles fontStyle = FontStyles.Bold;
        public float fontSize = 47f;
    }
}