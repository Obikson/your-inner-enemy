﻿using UnityEngine;

namespace Plugins.TobeCore.UI
{
    public static class GameObjectHelpers
    {
        public static void DestroyAllChildren(this Transform transform)
        {
            var count = transform.childCount;
            for (int i = 0; i < count; i++)
            {
                Object.Destroy(transform.GetChild(i).gameObject);
            }
        }
    }
}