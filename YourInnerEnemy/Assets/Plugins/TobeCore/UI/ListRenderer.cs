﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Object = UnityEngine.Object;

namespace Plugins.TobeCore.UI
{
    public abstract class ListRenderer<TDataModel, TListPrefab> : MonoBehaviour where TListPrefab : MonoBehaviour
    {
        public GameObject container;

        public TListPrefab prefab;

        public List<TListPrefab> options = new List<TListPrefab>();

        public void Clear(bool destroyGameObjects = true)
        {
            if (destroyGameObjects)
            {
                foreach (var option in options)
                {
                    Destroy(option.gameObject);
                }
            }

            options.Clear();
        }

        public virtual void Bind(TDataModel[] items)
        {
            Clear();

            for (var i = 0; i < items.Length; i++)
            {
                Instantiate(prefab, container.transform);
            }
        }

        protected readonly string[] optionAlphaIndexes = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K"};

        public void Bind(TDataModel[] items, Action<int, TDataModel, TListPrefab> bindAction)
        {
            for (var i = 0; i < items.Length; i++)
            {
                var dataModel = items[i];

                var itemPrefab = Instantiate(prefab, container.transform);

                bindAction(i, dataModel, itemPrefab);

                options.Add(itemPrefab);
            }
        }

        public virtual void Bind(TDataModel[] items, UnityAction<TDataModel, TListPrefab> onClick)
        {
            for (var i = 0; i < items.Length; i++)
            {
                var dataModel = items[i];

                var instance = Instantiate(prefab, container.transform);

                onClick(dataModel, instance);
                options.Add(instance);
            }
        }
    }
}