﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Plugins.TobeCore.UI
{
    public class MdIcon : MdElement
    {
        public Image graphics;
        
        public void SetFontColor(Color color)
        {
            graphics.color = color;
        }

        public string icon = "menu";

        public void SetIcon(string iconKey)
        {
            icon = iconKey;
            Refresh();
        }

        private MdElement _parentElement;

        public override void Refresh()
        {
            base.Refresh();
            var parentEl = transform.parent.GetComponent<MdElement>();
            if (parentEl != this && (!_parentElement || parentEl != _parentElement))
                _parentElement = parentEl;
            
            
            graphics.sprite = MdSettings.Settings.iconSet.GetIcon(icon);
            graphics.color = _parentElement && _parentElement.isDark ? MdSettings.Settings.fontSettings.fontColorLight : MdSettings.Settings.fontSettings.fontColorDark;
        }
    }
}