﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Plugins.TobeCore.UI
{
    public class MdPagingItem : MdElement
    {
        public Image background;
        public bool isActive;
        public Color activeColor;
        public Color visitedColor;
        public bool visited;
        public int number;
        public MdText text;

        private void Start()
        {
            
        }

        public void SetNumber(int value)
        {
            number = value;
            text.SetText(number.ToString());
        }

        public void Activate()
        {
            isActive = true;
            background.color = activeColor;
        }

        public void Visit()
        {
            if (!isActive) return;
            visited = true;
            background.color = visitedColor;
        }
    }
}