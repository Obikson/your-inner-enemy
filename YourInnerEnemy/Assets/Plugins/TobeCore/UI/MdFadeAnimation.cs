﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Plugins.TobeCore.UI
{
    public enum FadeDirection
    {
        None = 0,
        In = 1,
        Out = 2
    }
}