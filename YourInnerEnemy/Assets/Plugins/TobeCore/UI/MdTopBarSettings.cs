﻿using System;
using UnityEngine;

namespace Plugins.TobeCore.UI
{
    [Serializable]
    public class MdTopBarSettings 
    {
        public float topBarHeight = 100;
    }
}