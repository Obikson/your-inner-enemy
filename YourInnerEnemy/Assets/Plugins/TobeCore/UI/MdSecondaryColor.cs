﻿using UnityEngine.UI;

namespace Plugins.TobeCore.UI
{
    public class MdSecondaryColor : MdElement
    {
        public Image image;

        public override void Refresh()
        {
            base.Refresh();
            image.color = MdSettings.Settings.currentPalette.secondaryColor;
        }
    }
}