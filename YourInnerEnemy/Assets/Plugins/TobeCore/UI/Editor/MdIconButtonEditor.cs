﻿using UnityEditor;
using UnityEngine;

namespace Plugins.TobeCore.UI.Editor
{
    [CustomEditor(typeof(MdIconButton))]
    public class MdIconButtonEditor : MdElementEditor<MdIconButton>
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
         
        }
    }[CustomEditor(typeof(MdAnimation))]
    public class MdAnimationEditor : EditorType<MdAnimation>
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
         
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Set Slide start position (optional)"))
            {
               TargetTyped.SlideIn();
            }

            if (GUILayout.Button("Set Slide end position"))
            {
                TargetTyped.SlideOut();
            }

            GUILayout.EndHorizontal();
        }
    }
}
