﻿using UnityEngine;

namespace Plugins.TobeCore.UI.Editor
{
    public abstract class EditorType<TModel> : UnityEditor.Editor where TModel : class
    {
        protected TModel TargetTyped => target as TModel;

        protected string TextField(string text, params GUILayoutOption[] options)
        {
            return GUILayout.TextField(text, options);
        }
    }
}