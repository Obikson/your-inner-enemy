﻿using Plugins.TobeCore.UI.Layout;
using UnityEditor;
using UnityEngine;

namespace Plugins.TobeCore.UI.Editor
{
    [CustomEditor(typeof(MdPanel))]
    public class MdPanelEditor : EditorType<MdPanel>
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            GUILayout.BeginHorizontal();
            
            if (GUILayout.Button("Show"))
            {
                
                TargetTyped.ToggleVisibility(true, MdLayoutAnimationType.FadeIn);
            }
            
            if (GUILayout.Button("Hide"))
            {
                
                TargetTyped.ToggleVisibility(false, MdLayoutAnimationType.FadeOut);
            }
            
            GUILayout.EndHorizontal();
        }
    }
}