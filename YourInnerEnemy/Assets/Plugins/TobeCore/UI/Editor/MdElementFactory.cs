﻿using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Plugins.TobeCore.UI.Editor
{
    public static class MdElementFactory
    {
        [MenuItem("GameObject/UI/Material Design/Button", false, 0)]
        public static void MdButton()
        {
            var result = new GameObject("btn_md_button");


            result.layer = LayerMask.NameToLayer("UI");


            var btn = result.AddComponent<Button>();

            var image = result.AddComponent<Image>();

            image.sprite = MdSettings.Settings.buttonSettings.lgButtonBackground;
            image.type = Image.Type.Tiled;
            btn.targetGraphic = image;

            btn.transition = Selectable.Transition.ColorTint;

            result.transform.parent = Selection.activeGameObject.transform;


            var mdButton = result.AddComponent<MdButton>();
            mdButton.graphics = image;
            var labelGo = new GameObject("lbl_text");


            var label = labelGo.AddComponent<TextMeshProUGUI>();
            label.alignment = TextAlignmentOptions.Center;
            mdButton.label = label;

            label.transform.parent = result.transform;


            result.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
            result.GetComponent<RectTransform>().sizeDelta = new Vector2(300, 100);
            mdButton.button = btn;

            mdButton.SetText("Md Button");
            mdButton.Refresh();

            Selection.activeGameObject = result;
        }

        [MenuItem("GameObject/UI/Material Design/Icon Button", false, 0)]
        public static void MdIconButton()
        {
            var result = new GameObject("btn_md_icon", typeof(RectTransform))
            {
                layer = LayerMask.NameToLayer("UI")
            };


            var btn = result.AddComponent<Button>();

            var innerImage = new GameObject("icon", typeof(RectTransform));
            
            innerImage.transform.SetParent(result.transform);
            
            innerImage.transform.StretchMax();

            var image = innerImage.AddComponent<Image>();

            image.type = Image.Type.Simple;

            btn.targetGraphic = image;

            btn.transition = Selectable.Transition.ColorTint;

            result.transform.SetParent(Selection.activeGameObject.transform);

            var mdButton = result.AddComponent<MdIconButton>();

            mdButton.graphics = image;

            result.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;

            result.GetComponent<RectTransform>().sizeDelta = new Vector2(100, 100);

            mdButton.button = btn;

            mdButton.Refresh();

            Selection.activeGameObject = result;
        }

        [MenuItem("GameObject/UI/Material Design/Top Bar", false, 0)]
        public static void MdTopBar()
        {
            var result = new GameObject("pnl_md_top_bar");
            result.layer = LayerMask.NameToLayer("UI");

            var image = result.AddComponent<Image>();

            image.type = Image.Type.Simple;

            result.transform.parent = Selection.activeGameObject.transform;


            var mdTopBar = result.AddComponent<MdTopBar>();
            mdTopBar.image = image;
            mdTopBar.Refresh();

            Selection.activeGameObject = result;
        }

        [MenuItem("GameObject/UI/Material Design/Side Menu", false, 0)]
        public static void MdSideMenu()
        {
            var result = new GameObject("pnl_md_side_menu", typeof(RectTransform));

            result.layer = LayerMask.NameToLayer("UI");

            result.transform.parent = Selection.activeGameObject.transform;
            result.transform.StretchMax();

            var content = new GameObject("pnl_content", typeof(RectTransform));
            content.transform.parent = result.transform;

            var contentrect = content.GetComponent<RectTransform>();
            content.AddComponent<Image>();
            contentrect.anchorMin = Vector2.zero;
            contentrect.anchorMax = new Vector2(0.8f, 1);
            contentrect.sizeDelta = Vector2.zero;

            var vertGroup = content.AddComponent<VerticalLayoutGroup>();
            vertGroup.childControlWidth = true;
            vertGroup.childForceExpandWidth = true;
            vertGroup.childForceExpandHeight = false;

            var header = new GameObject("pnl_header");
            header.transform.parent = content.transform;

            var headerRect = header.AddComponent<RectTransform>();

            var container = new GameObject("pnl_container");
            container.transform.parent = content.transform;

            var listContainer = container.AddComponent<RectTransform>();

            var closeOverlay = new GameObject("img_overlay");
            closeOverlay.transform.parent = result.transform;
            var closeButton = closeOverlay.AddComponent<Button>();
            closeButton.transition = Selectable.Transition.None;
            var closeRect = closeOverlay.AddComponent<RectTransform>();
            closeRect.anchorMin = new Vector2(0.8f, 0);
            closeRect.anchorMax = new Vector2(1f, 1);
            closeRect.sizeDelta = Vector2.zero;
            var mdSideMenu = result.AddComponent<MdSideBar>();
            mdSideMenu.itemsContainer = listContainer;
            mdSideMenu.headerContainer = headerRect;
            closeButton.onClick.AddListener(() => { Debug.Log("click"); });
            var image = closeOverlay.AddComponent<Image>();
            image.type = Image.Type.Simple;

            var color = image.color;

            color.a = .6f;

            image.color = color;

            mdSideMenu.image = image;
            mdSideMenu.Refresh();


            var topBars = Object.FindObjectsOfType<MdTopBar>();

            foreach (var topBar in topBars)
            {
                if (topBar.sideBar) continue;
                topBar.sideBar = mdSideMenu;
            }


            Selection.activeGameObject = result;
        }

        [MenuItem("GameObject/UI/Material Design/Input", false, 0)]
        public static void MdInput()
        {
            var result = new GameObject("txt_md_input") {layer = LayerMask.NameToLayer("UI")};
            result.transform.parent = Selection.activeGameObject.transform;


            var input = result.AddComponent<MdInput>();
            var image = result.AddComponent<Image>();
            image.type = Image.Type.Tiled;
            image.sprite = MdSettings.Settings.inputSettings.bgInput;
            var tmpInput = result.AddComponent<TMP_InputField>();


            var textArea = new GameObject("tmp_mask", typeof(RectMask2D));
            textArea.transform.parent = result.transform;
            textArea.transform.StretchMax();
            tmpInput.textViewport = textArea.GetComponent<RectTransform>();

            var placeholder = new GameObject("tmp_placeholder");
            var placeholderTmp = placeholder.AddComponent<TextMeshProUGUI>();
            placeholder.transform.parent = textArea.transform;
            placeholder.transform.StretchMax();
            tmpInput.placeholder = placeholderTmp;

            var text = new GameObject("tmp_text");
            text.transform.parent = textArea.transform;
            var textTmp = text.AddComponent<TextMeshProUGUI>();
            text.transform.StretchMax();

            var underline = new GameObject("img_underline", typeof(Image));
            underline.transform.parent = result.transform;
            var underlineRect = underline.transform as RectTransform;
            underlineRect.anchorMin = Vector2.zero;
            underlineRect.anchorMax = new Vector2(1, 0);
            underlineRect.sizeDelta = new Vector2(0, 10);


            tmpInput.textComponent = textTmp;
            tmpInput.fontAsset = MdSettings.Settings.fontSettings.fontNormal;

            input.tmpInput = tmpInput;
            input.tmpPlaceholder = placeholderTmp;
            input.tmpText = textTmp;
            input.underline = underline.GetComponent<Image>();
            input.Refresh();
            Selection.activeGameObject = result;
        }

        [MenuItem("GameObject/UI/Material Design/Toggle", false, 0)]
        public static void MdCheckbox()
        {
            var result = new GameObject("tgl_md_check_box", typeof(RectTransform))
                {layer = LayerMask.NameToLayer("UI")};


            result.transform.parent = Selection.activeGameObject.transform;
            var rectTransform = result.GetComponent<RectTransform>();

            rectTransform.anchoredPosition = Vector2.zero;


            var hlg = result.AddComponent<HorizontalLayoutGroup>();
            hlg.childForceExpandHeight = true;
            hlg.childForceExpandWidth = false;
            hlg.childControlHeight = true;
            hlg.childControlWidth = false;

            var checkbox = result.AddComponent<MdCheckbox>();

            checkbox.toggle = result.AddComponent<Toggle>();

            var checkBackground = new GameObject("img_background");

            checkBackground.transform.parent = result.transform;

            var bgImage = checkBackground.AddComponent<Image>();

            var checkIcon = new GameObject("img_check_image");

            var checkIconImage = checkIcon.AddComponent<Image>();

            checkIcon.transform.parent = checkBackground.transform;

            checkIcon.transform.StretchMax();

            var label = new GameObject("tmp_label");

            label.transform.parent = result.transform;

            checkbox.tmpLabel = label.AddComponent<TextMeshProUGUI>();

            checkbox.tmpLabel.alignment = TextAlignmentOptions.Left;

            label.transform.StretchMax();

            var labelRectTransf = label.GetComponent<RectTransform>();

            labelRectTransf.sizeDelta = new Vector2(500, 500);

            checkbox.tmpLabel.text = "Toggle";

            checkbox.toggle.graphic = checkIconImage;

            checkbox.toggle.image = bgImage;

            checkbox.Refresh();

            Selection.activeGameObject = result;
        }

        [MenuItem("GameObject/UI/Material Design/Container", false, 0)]
        public static void MdContainer()
        {
            var result = new GameObject("pnl_container", typeof(RectTransform)) {layer = LayerMask.NameToLayer("UI")};
            result.transform.parent = Selection.activeGameObject.transform;
            result.transform.StretchMax();
            var rect = result.GetComponent<RectTransform>();

            rect.anchoredPosition = Vector2.zero;
            var container = result.AddComponent<MdContainer>();

            container.background = result.AddComponent<Image>();

            var content = new GameObject("pnl_content", typeof(RectTransform));
            content.transform.parent = result.transform;

            container.transform.StretchMax();

            container.content = content.transform as RectTransform;

            container.Refresh();
            Selection.activeGameObject = result;
        }

        [MenuItem("GameObject/UI/Material Design/Text", false, 0)]
        public static void MdText()
        {
            var result = new GameObject("lbl_md_text", typeof(RectTransform)) {layer = LayerMask.NameToLayer("UI")};

            result.transform.parent = Selection.activeGameObject.transform;

            var rect = result.GetComponent<RectTransform>();
            rect.anchoredPosition = Vector2.zero;

            rect.sizeDelta = new Vector2(300, 100);
            var container = result.AddComponent<MdText>();
            container.tmpLabel = result.AddComponent<TextMeshProUGUI>();

            container.tmpLabel.text = "Text";
            container.Refresh();

            Selection.activeGameObject = result;
        }


        [MenuItem("GameObject/UI/Material Design/FAB", false, 0)]
        public static void MdFab()
        {
            var result = new GameObject("btn_fab", typeof(RectTransform)) {layer = LayerMask.NameToLayer("UI")};

            result.transform.SetParent(Selection.activeGameObject.transform);
            ;


            var rect = result.GetComponent<RectTransform>();
            rect.anchoredPosition = Vector2.zero;
            rect.sizeDelta = new Vector2(100, 100);

            result.AddComponent<Button>();

            var container = result.AddComponent<MdFab>();

            container.background = result.AddComponent<Image>();
            container.isDark = true;

            var mdIcon = MdGameObjectFactory.GetMdIcon(container.transform);

            mdIcon.Refresh();

            container.mdIcon = mdIcon;

            container.Refresh();

            Selection.activeGameObject = result;
        }
    }

    public static class MdGameObjectFactory
    {
        public static MdIcon GetMdIcon(Transform parent = null, string iconSymbol = "menu", string goName = "md_icon")
        {
            var gameObject = new GameObject(goName, typeof(RectTransform));

            gameObject.transform.SetParent(parent ? parent : Selection.activeGameObject.transform);
            gameObject.transform.CenterAnchored();


            var icon = gameObject.AddComponent<MdIcon>();
            icon.graphics = gameObject.AddComponent<Image>();
            icon.graphics.preserveAspect = true;
            icon.graphics.raycastTarget = false;
            icon.icon = iconSymbol;
            return icon;
        }
    }
}