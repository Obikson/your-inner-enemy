﻿using System;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Plugins.TobeCore.UI.Editor
{
    public abstract class MdElementEditor<TMdModel> : EditorType<TMdModel> where TMdModel : MdElement
    {
        public override void OnInspectorGUI()
        {
            GUILayout.BeginHorizontal();

            GUILayout.Label("Settings");
            if (GUILayout.Button(new GUIContent("\u2622", "Apply all MD settings"), GUILayout.Width(30)))
            {
                var pColorObjects = FindObjectsOfType<MonoBehaviour>().OfType<IMdInitializable>();

                foreach (var primaryColor in pColorObjects)
                {
                    primaryColor.Refresh();
                    EditorUtility.SetDirty(primaryColor as MonoBehaviour);
                }
            }

            if (GUILayout.Button(new GUIContent("\u21b0", "Show all settings"), GUILayout.Width(30)))
            {
                Selection.activeObject = MdSettings.Settings;
            }

            if (GUILayout.Button(new GUIContent("\u21ba", "Refresh this component"), GUILayout.Width(30)))
            {
                (TargetTyped as MdElement)?.Refresh();
            }

            if (GUILayout.Button(new GUIContent(typeof(TMdModel).Name, typeof(TMdModel).Name + " settings")))
            {
                Selection.activeObject = InspectorObjectSettings();
            }

            GUILayout.EndHorizontal();



            base.OnInspectorGUI();
        }

        protected virtual Object InspectorObjectSettings()
        {
            return MdSettings.Settings;
        }
    }


    public abstract class AbstractSettingsEditor<TModel> : EditorType<TModel> where TModel : class
    {
        public override void OnInspectorGUI()
        {
            GUILayout.BeginHorizontal();

            GUILayout.Label("Settings");
            if (GUILayout.Button(new GUIContent("\u2622", "Apply all MD settings"), GUILayout.Width(30)))
            {
                var pColorObjects = FindObjectsOfType<MonoBehaviour>().OfType<IMdInitializable>();

                foreach (var primaryColor in pColorObjects)
                {
                    primaryColor.Refresh();
                    EditorUtility.SetDirty(primaryColor as MonoBehaviour);
                }
            }

            if (GUILayout.Button(new GUIContent("\u21b0", "Show all settings"), GUILayout.Width(30)))
            {
                Selection.activeObject = MdSettings.Settings;
            }

            GUILayout.EndHorizontal();

            base.OnInspectorGUI();
        }
    }

    [CustomEditor(typeof(MdPaletteScriptableObject))]
    public class MdPaletteEditor : AbstractSettingsEditor<MdPaletteScriptableObject>
    {
    }

    [CustomEditor(typeof(MdSideBar))]
    public class MdSideBarEditor : MdElementEditor<MdSideBar>
    {
    }

    [CustomEditor(typeof(MdTopBar))]
    public class MdTopBarEditor : MdElementEditor<MdTopBar>
    {
    }

    [CustomEditor(typeof(MdCheckbox))]
    public class MdCheckboxEditor : MdElementEditor<MdCheckbox>
    {
    }

    [CustomEditor(typeof(MdContainer))]
    public class MdContainerEditor : MdElementEditor<MdContainer>
    {
    }

    [CustomEditor(typeof(MdFab))]
    public class MdMdFabEditor : MdElementEditor<MdFab>
    {
    }

    [CustomEditor(typeof(MdButton))]
    public class MdButtonEditor : MdElementEditor<MdButton>
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.BeginHorizontal();

            GUILayout.Label("Button Text", GUILayout.Width(110));

            var text = GUILayout.TextField(TargetTyped.GetText());

            EditorGUILayout.EndHorizontal();

            TargetTyped.SetText(text);
        }
    }


    [CustomEditor(typeof(MdInput))]
    public class MdInputEditor : MdElementEditor<MdInput>
    {
    }
}