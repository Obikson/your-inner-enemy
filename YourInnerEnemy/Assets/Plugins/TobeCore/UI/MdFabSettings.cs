﻿using System;
using UnityEngine;

namespace Plugins.TobeCore.UI
{
    [Serializable]
    public class MdFabSettings
    {
        public Sprite background;
    }
}