﻿using System;
using TMPro;
using UnityEngine;

namespace Plugins.TobeCore.UI
{
    [Serializable]
    public class MdFontSettings
    {
        public Color fontColorDark = Color.black;
        public Color fontColorLight = Color.white;
        public TMP_FontAsset fontNormal;
        public float largeSize = 50;
        public float normalSize = 36;
        public float smallSize = 26;
    }
}