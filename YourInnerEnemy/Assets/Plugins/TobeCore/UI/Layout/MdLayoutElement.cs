﻿using System;
using UnityEngine;
using UnityEngine.Events;
using Object = UnityEngine.Object;

namespace Plugins.TobeCore.UI.Layout
{
    public enum MdLayoutAnimationType
    {
        None = 0,
        EaseIn = 1,
        EaseOut = 2,
        FadeIn = 3,
        FadeOut = 4
    }

    public abstract class MdLayoutElement : MonoBehaviour, IMdLayoutElement
    {
        [SerializeField] private UnityEvent onVisibilityChanged;

        [SerializeField] private bool _isActive = true;

        [SerializeField] private bool _isVisible = true;

        private MdLayoutAnimationType? _animationType;

        public bool IsActive
        {
            get => _isActive;
            private set => _isActive = value;
        }

        public bool IsVisible
        {
            get => _isVisible;
            private set => _isVisible = value;
        }


        public virtual void ToggleActive(bool value)
        {
            IsActive = value;

            gameObject.SetActive(IsActive);
        }

        public virtual void ToggleActive()
        {
            IsActive = !IsActive;
        }


        public virtual void ToggleVisibility(bool value)
        {
            if (value == _isVisible)
                return;

            IsVisible = value;
            AnimationType = MdLayoutAnimationType.None;
            InvokeVisibilityChanged();
        }

        public void ToggleVisibility(bool value, MdLayoutAnimationType animationType)
        {
            if (value == _isVisible)
                return;

            ToggleVisibility(value);

            AnimationType = animationType;
             AnimationUpdate();
            InvokeVisibilityChanged();
        }

        public virtual void ToggleVisibility()
        {
            IsVisible = !IsVisible;
            AnimationType = MdLayoutAnimationType.None;
            InvokeVisibilityChanged();
        }

        public UnityEvent OnVisibilityChanged => onVisibilityChanged;

        protected virtual void InvokeVisibilityChanged()
        {
            OnVisibilityChanged?.Invoke();
        }

        protected virtual void Start()
        {
            CancelAnimation();
        }

        protected TComponent GetComponentRequired<TComponent>() where TComponent : Object
        {
            var result = GetComponent<TComponent>();
            if (!result)
            {
                Debug.LogError("Required component is missing " + result);
                return null;
            }
            else
            {
                return result;
            }
        }

        public float AnimationProgress { get; set; }

        public MdLayoutAnimationType? AnimationType
        {
            get => _animationType;
            set
            {
                AnimationProgress = 0;
                _animationType = value;
            }
        }

        protected virtual void Update()
        {
            if (AnimationType != null)
            {
                AnimationUpdate();
            }
        }

        protected virtual void AnimationUpdate()
        {
        }

        public void CancelAnimation()
        {
            AnimationType = null;
            AnimationProgress = 0;
            AnimationUpdate();
        }
    }

    public static class MdLayoutAnimationService
    {
        public static void ProcessAnimation(MdPanel panel)
        {
            if (panel.AnimationType == null || panel.AnimationType == MdLayoutAnimationType.None)
            {
                panel.canvasGroup.alpha = panel.IsVisible ? 1 : 0;
                return;
            }

            if (panel.AnimationType == MdLayoutAnimationType.FadeIn ||
                panel.AnimationType == MdLayoutAnimationType.FadeOut)
            {
                var curve = MdLayoutElementAnimations.MdPanelFading();

                var speed = MdLayoutElementAnimations.FadingSpeed * Time.deltaTime;

                panel.AnimationProgress += speed;

                if (panel.AnimationProgress >= 1)
                {
                    panel.CancelAnimation();
                    return;
                }

                var opacity = panel.AnimationType == MdLayoutAnimationType.FadeOut
                    ? 1 - curve.Evaluate(panel.AnimationProgress)
                    : curve.Evaluate(panel.AnimationProgress);

                panel.canvasGroup.alpha = opacity;
            }
        }
    }
}