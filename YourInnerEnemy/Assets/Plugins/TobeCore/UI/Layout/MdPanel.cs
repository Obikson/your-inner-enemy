﻿using System;
using UnityEngine;

namespace Plugins.TobeCore.UI.Layout
{
    public static class MdLayoutElementAnimations
    {
        public static AnimationCurve MdPanelFading()
        {
            return AnimationCurve.EaseInOut(0, 0, 1, 1);
        }

        public static float FadingSpeed => 1f;
    }

    [RequireComponent(typeof(CanvasGroup))]
    public class MdPanel : MdLayoutElement
    {
        public CanvasGroup canvasGroup;

        protected override void Start()
        {
            canvasGroup = GetComponentRequired<CanvasGroup>();

            canvasGroup.alpha = IsVisible ? 1 : 0;
            canvasGroup.interactable = IsVisible;
            canvasGroup.blocksRaycasts = IsVisible;
            base.Start();
        }

        public override void ToggleVisibility(bool value)
        {
            base.ToggleVisibility(value);
            canvasGroup.interactable = IsVisible;
            canvasGroup.blocksRaycasts = IsVisible;
        }

        protected override void AnimationUpdate()
        {
            MdLayoutAnimationService.ProcessAnimation(this);
        }
    }
}