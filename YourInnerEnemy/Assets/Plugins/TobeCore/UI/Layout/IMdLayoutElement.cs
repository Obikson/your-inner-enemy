﻿using UnityEngine.Events;

namespace Plugins.TobeCore.UI.Layout
{
    public interface IMdLayoutElement
    {
        bool IsActive { get; }

        void ToggleActive(bool value);
        
        void ToggleActive();
        
        bool IsVisible { get; }

        void ToggleVisibility(bool value);
        void ToggleVisibility(bool value, MdLayoutAnimationType animationType);
        
        void ToggleVisibility();

        UnityEvent OnVisibilityChanged { get; }
    }
}