﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Plugins.TobeCore.UI
{
    [RequireComponent(typeof(Button))]
    public class MdTooltip : MdElement
    {
        public MdAnimation fadeAnimation;
        public Button button;
        public MdText label;

        void Start()
        {
            button.onClick.AddListener(() => { fadeAnimation.FadeIn(); });
        }

        public void Show()
        {
            if(_coroutine != null)
                StopCoroutine(_coroutine);
            
            fadeAnimation.FadeIn();

            _coroutine = StartCoroutine(Delay());
        }

        public void Hide()
        {
            fadeAnimation.FadeOut();
        }

        private Coroutine _coroutine;
        
        private IEnumerator Delay()
        {
            yield return new WaitForSecondsRealtime(5);
            
            Hide();
        }
    }
}