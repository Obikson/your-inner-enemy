﻿using System;
using TMPro;
using UnityEngine.UI;

namespace Plugins.TobeCore.UI
{
    public class MdCheckbox : MdElement, IMdCheckbox, IMdFormField
    {
        public Toggle toggle;
        public TextMeshProUGUI tmpLabel;
        
        public override void Refresh()
        {
            base.Refresh();
            toggle.image.sprite = MdSettings.Settings.checkboxSettings.checkboxEmpty;
            (toggle.graphic as Image).sprite = MdSettings.Settings.checkboxSettings.checkboxChecked;
            tmpLabel.margin = MdSettings.Settings.checkboxSettings.checkboxLabelMargin;

        }

        public string Text
        {
            get => tmpLabel.text;
            set => tmpLabel.text = value;
        }

        public bool IsValid { get; } = true;
        
        public string Value
        {
            get => toggle.isOn.ToString();
            set => toggle.isOn = !string.IsNullOrEmpty(value) && value.Equals("true", StringComparison.InvariantCulture);
        }
        
        public string Name { get; set; }
        
        public TValue GetValue<TValue>()
        {
            throw new System.NotImplementedException();
        }
    }
}