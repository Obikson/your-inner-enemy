﻿using UnityEngine;
using UnityEngine.UI;

namespace Plugins.TobeCore.UI
{
    
    public class MdFab : MdElement
    {
        public string icon = "menu";

        public MdIcon mdIcon;
        public Image background;
        public override void Refresh()
        {
            base.Refresh();
            
            background.color = MdSettings.Settings.currentPalette.accend;
            background.sprite = MdSettings.Settings.fab.background;
            
            mdIcon.icon = icon;
        }
    }
}