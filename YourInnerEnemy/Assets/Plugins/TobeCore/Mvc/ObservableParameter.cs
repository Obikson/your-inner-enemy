﻿using System;

namespace Plugins.TobeCore.Mvc
{
    public class ObservableParameter<TType>
    {
        public TType Value
        {
            get => _value;
            set
            {
                if(_value.Equals(value))
                    return;
                
                _value = value;
                
                OnChanged(_value);
            }
        }

        public event EventHandler<TType> Changed;

        private TType _value;

        public ObservableParameter(TType value)
        {
            _value = value;
        }

        public void SetSilently(TType value)
        {
            _value = value;
        }
        
        protected virtual void OnChanged(TType e)
        {
            Changed?.Invoke(this, e);
        }
    }
}