﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Plugins.TobeCore.Mvc
{
    public interface IAssetRenderer
    {
        void PlayState(string stateName);

        DataModel DataModel { get; }
    }

    public class AssetRenderer<TDataModel> : AssetRendererBase where TDataModel : DataModel
    {
        [SerializeField] private GameObject[] models;

        [Inject]
        private IAssetRendererService _assetRendererService;

        public GameObject[] Models => models;
        
        public Renderer[] Renderers { get; private set; }

        public Animator animator;

        protected virtual void Awake()
        {
            var renderers = new List<Renderer>();

            foreach (var model in models)
            {
                renderers.AddRange(model.GetComponentsInChildren<Renderer>());
            }
            
            Renderers = renderers.ToArray();
            
            animator = GetComponent(animator);
        }

        private TDataModel _viewModel;

        public override DataModel DataModel => _viewModel;

        public virtual void BindModel(TDataModel viewModel)
        {
            _viewModel = viewModel;
            
           viewModel.IsVisible.Changed += IsVisibleOnChanged;
           
           viewModel.IsActive.Changed += IsActiveOnChanged;
           
           _assetRendererService.ToggleVisible(this, viewModel.IsVisible.Value);
           
           _assetRendererService.SetActive(this, viewModel.IsActive.Value);
        }

        private void IsActiveOnChanged(object sender, bool e)
        {
            _assetRendererService.SetActive(this, e);
        }

        private void IsVisibleOnChanged(object sender, bool e)
        {
            _assetRendererService.ToggleVisible(this, e);
        }
        
        protected TComponent GetComponent<TComponent>(TComponent currentVal) where TComponent : Component
        {
            return currentVal ? currentVal : GetComponent<TComponent>();
        }

        public override void PlayState(string stateName)
        {
            print("Play state " + stateName);
            
            animator.Play(stateName);
        }

        public void SetActive(bool value)
        {
            _assetRendererService.SetActive(this, value);
        }
    }
}