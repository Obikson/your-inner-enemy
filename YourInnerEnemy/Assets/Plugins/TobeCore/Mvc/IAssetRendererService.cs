﻿namespace Plugins.TobeCore.Mvc
{
    public interface IAssetRendererService
    {
        void ToggleVisible<TDataModel>(AssetRenderer<TDataModel> assetRenderer, bool value)
            where TDataModel : DataModel;

        void SetActive<TDataModel>(AssetRenderer<TDataModel> assetRenderer, bool value) where TDataModel : DataModel;
    }
}