﻿using System;
using UnityEngine;

namespace Plugins.TobeCore.Mvc
{
    public abstract class DataModel : MonoBehaviour
    {
        public bool isActive = true;

        public bool isVisible = true;
        
        public ObservableParameter<bool> IsVisible { get; private set; }

        public ObservableParameter<bool> IsActive { get; private set; }

        protected virtual void Awake()
        {
            IsVisible = new ObservableParameter<bool>(isVisible);
            IsActive = new ObservableParameter<bool>(isActive);
        }
    
        public void SetActive(bool value)
        {
            IsActive.Value = value;
        }

        public void SetVisible(bool value)
        {
            IsVisible.Value = value;
        }
    }
}