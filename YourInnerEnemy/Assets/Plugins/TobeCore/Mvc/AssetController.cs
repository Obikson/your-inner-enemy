﻿using UnityEngine;

namespace Plugins.TobeCore.Mvc
{
    public abstract class AssetController<TDataModel> : MonoBehaviour where TDataModel : DataModel
    {
        public TDataModel dataModel;

        public AssetRenderer<TDataModel> AssetRenderer { get; private set; }


        protected virtual void Start()
        {
            AssetRenderer = GetComponentInChildren<AssetRenderer<TDataModel>>();
            
            InitializeRenderer(AssetRenderer);
        }

        protected virtual void InitializeRenderer(AssetRenderer<TDataModel> assetRenderer)
        {
            assetRenderer.BindModel(dataModel);
        }

        protected TComponent GetComponent<TComponent>(TComponent currentVal) where TComponent : Component
        {
            return currentVal ? currentVal : GetComponent<TComponent>();
        }
        
        
    }
}