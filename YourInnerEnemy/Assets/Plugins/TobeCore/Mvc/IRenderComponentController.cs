﻿namespace Plugins.TobeCore.Mvc
{
    public interface IRenderComponentController<in TViewModel>
    {
        void Update(TViewModel model);
    }
}