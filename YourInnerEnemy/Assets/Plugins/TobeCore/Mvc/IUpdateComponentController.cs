﻿namespace Plugins.TobeCore.Mvc
{
    
    
    public interface IUpdateComponentController<in TUpdateComponent> 
    {
        void Update(TUpdateComponent component);
    }
}