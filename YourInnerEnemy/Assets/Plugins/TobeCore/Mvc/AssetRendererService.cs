﻿namespace Plugins.TobeCore.Mvc
{
    public class AssetRendererService : IAssetRendererService
    {
        public void ToggleVisible<TViewModel>(AssetRenderer<TViewModel> assetRenderer, bool value) where TViewModel : DataModel
        {
            assetRenderer.DataModel.IsVisible.SetSilently(value);
            
            foreach (var assetRendererRenderer in assetRenderer.Renderers)
            {
                assetRendererRenderer.enabled = value;
            }
        }

        public void SetActive<TViewModel>(AssetRenderer<TViewModel> assetRenderer, bool value) where TViewModel : DataModel
        {
            assetRenderer.DataModel.IsActive.SetSilently(value);
            
            foreach (var model in assetRenderer.Models)
            {
                model.SetActive(value);
            }
        }
    }
}