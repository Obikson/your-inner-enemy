﻿using Zenject;

namespace Plugins.TobeCore.Mvc
{
    public class MvcInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<AssetRendererService>().AsSingle();
        }
    }
}