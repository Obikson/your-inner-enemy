﻿using UnityEngine;

namespace Plugins.TobeCore.Mvc
{
    public abstract class AssetRendererBase :  MonoBehaviour, IAssetRenderer
    {
        public abstract void PlayState(string stateName);

        public abstract DataModel DataModel { get; }
    }
}