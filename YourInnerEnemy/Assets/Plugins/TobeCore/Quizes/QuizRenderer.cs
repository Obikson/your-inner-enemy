﻿using System;
using System.Linq;
using Plugins.TobeCore.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Plugins.TobeCore.Quizes
{
    public class QuizRenderer : MonoBehaviour
    {
        public TextMeshProUGUI questionDisplay;
        public TextMeshProUGUI helpText;
        public QuizManager quizManager;

        public RectTransform answersContainer;

        public Button checkboxPrefab;

        public Button startButton;

        public int correctAnswersInCurrentQuestion;

        public RectTransform resultPanel;
        public TextMeshProUGUI resultText;
        public MdPagingItem pagingPrefab;
        public RectTransform pagingContainer;
        public Color correctColor;
        public Color invalidColor;

        public void RefreshQuizLayout()
        {
            var question = quizManager.GetCurrentQuestion();

            ClearContainer();

            PopulateContainer(question);
        }

        private void Start()
        {
            quizManager.onQuestionChanged.AddListener(RefreshQuizLayout);

            questionDisplay.text = quizManager.introTitle;
        }

        public void PopulateContainer(QuizQuestion question)
        {
            correctAnswersInCurrentQuestion = 0;


            questionDisplay.text = question.text;

            answersContainer.gameObject.SetActive(true);
            helpText.text = "";
//            helpText.text = "Otázka " + (quizManager.currentQuestion + 1) + "/" + quizManager.questions.Length +
//                            (question.GetCorrectAnswersCount() > 1
//                                ? "Tato otázka má více správných odpovědí"
//                                : string.Empty);

           //

            var index = 0;
            
            foreach (var questionAnswer in question.answers)
            {
                var checkbox = Instantiate(checkboxPrefab, answersContainer);
                index++;

                checkbox.GetComponent<QuizAnswerButton>().text.SetText(questionAnswer.unanswerText);
                checkbox.GetComponent<QuizAnswerButton>().answerSymbol
                    .SetText(index == 1 ? "A" : index == 2 ? "B" : "C");


                checkbox.onClick.AddListener(() =>
                {
                    if (questionAnswer.isCorrect)
                    {
                        quizManager.correctAnswers++;
                        correctAnswersInCurrentQuestion++;
                    }

                    checkbox.image.color = questionAnswer.isCorrect ? correctColor : invalidColor;

                    checkbox.GetComponent<QuizAnswerButton>().text.SetText(questionAnswer.answerText);

                    var correctAnswersCount = question.GetCorrectAnswersCount();
                    if (correctAnswersCount == correctAnswersInCurrentQuestion || correctAnswersCount == 1)
                    {
                        DisableAnswers();
                        quizManager.NextQuestion();
                        if (quizManager.nextQuestionDelay > 0 && quizManager.GetCurrentQuestion())
                        {
                            helpText.text = "Připravuji další otázku";
                        }
                    }
                });
            }
        }

        public void DisableAnswers()
        {
            var childs = answersContainer.childCount;

            for (int i = 0; i < childs; i++)
            {
                var child = answersContainer.GetChild(i);
                child.GetComponent<Button>().interactable = false;
            }
        }

        public void ClearContainer()
        {
            var childs = answersContainer.childCount;

            for (int i = 0; i < childs; i++)
            {
                var child = answersContainer.GetChild(i);
                Destroy(child.gameObject);
            }
        }

        public void ShowResult()
        {
            ClearContainer();
            answersContainer.gameObject.SetActive(false);
            resultPanel.gameObject.SetActive(true);
            helpText.text = "Konec testu";
            resultText.text = "V tomto testu máte úspěšnost " + quizManager.correctAnswers + " z " +
                              quizManager.questions.Length;
        }
    }
}