﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace Plugins.TobeCore.Quizes
{
    public class QuizManager : MonoBehaviour
    {
        public string introTitle;

        public QuizQuestion[] questions;

        public UnityEvent onQuizCompleted;
        public UnityEvent onQuizSuccess;
        public UnityEvent onQuizFailed;

        public UnityEvent onQuestionChanged;

        public void StartQuiz()
        {
            
            NextQuestion();
        }

        public QuizQuestion GetCurrentQuestion()
        {
            if (currentQuestion < 0 || currentQuestion >= questions.Length)
                return null;

            return questions[currentQuestion];
        }

        public int correctAnswers;

        public int currentQuestion;

        public float nextQuestionDelay;

        public int GetRating()
        {
            return currentQuestion <= 0 ? 0 : correctAnswers / currentQuestion;
        }

        public float GetProgress()
        {
            if (questions.Length == 0) return 0;
            
            return Mathf.Clamp01(currentQuestion / (float) questions.Length);
        }

        public void NextQuestion()
        {
            var next = currentQuestion + 1;

            

            currentQuestion = next;

            if (nextQuestionDelay > 0 && currentQuestion != 0)
            {
                if (_nextQuestionCoroutine != null)
                    StopCoroutine(_nextQuestionCoroutine);

                _nextQuestionCoroutine = StartCoroutine(NextQuestionCoroutine());
            }
            else
            {
                if (next >= questions.Length)
                {
                    CompleteQuiz();
                }
                else
                {
                    onQuestionChanged?.Invoke();
                }
            }
        }

        private Coroutine _nextQuestionCoroutine;

        private IEnumerator NextQuestionCoroutine()
        {
            yield return new WaitForSecondsRealtime(nextQuestionDelay);
            if (!GetCurrentQuestion())
            {
                CompleteQuiz();
            }
            else
            {
                onQuestionChanged?.Invoke();
            }
        }

        public void CompleteQuiz()
        {
            if (correctAnswers == questions.Sum(x => x.GetCorrectAnswersCount()))
            {
                onQuizSuccess?.Invoke();
            }
            else
            {
                onQuizFailed?.Invoke();
            }
            
            onQuizCompleted?.Invoke();
        }

        public void RestartQuiz()
        {
            currentQuestion = -1;
            correctAnswers = 0;
            StartQuiz();
        }
    }
}