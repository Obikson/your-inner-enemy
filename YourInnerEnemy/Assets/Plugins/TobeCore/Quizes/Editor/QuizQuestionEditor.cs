﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Plugins.TobeCore.Quizes.Editor
{
    public static class ArrayHelpers
    {
        public static void ShiftOneUp<T>(T[] array, int currentIndex)
        {
            if (currentIndex + 1 >= array.Length)
                return;

            var a = array[currentIndex];
            var b = array[currentIndex + 1];
            array[currentIndex] = b;
            array[currentIndex + 1] = a;
        }

        public static void ShiftOneDown<T>(T[] array, int currentIndex)
        {
            if (currentIndex - 1 < 0)
                return;

            var a = array[currentIndex];
            var b = array[currentIndex - 1];
            array[currentIndex] = b;
            array[currentIndex - 1] = a;
        }
    }

    [CustomEditor(typeof(QuizQuestion))]
    public class QuizQuestionEditor : UnityEditor.Editor
    {
        private QuizQuestion question => target as QuizQuestion;

        public override void OnInspectorGUI()
        {

            base.OnInspectorGUI();
            
//            EditorGUILayout.BeginHorizontal();
//
//            GUILayout.Label("Question is: ");
//            question.text = GUILayout.TextArea(question.text, GUILayout.Width(200),GUILayout.Height(60));
//            
//            if (GUILayout.Button("Add answer"))
//            {
//                var list = question.answers?.ToList() ?? new List<QuizAnswer>();
//                list.Add(new QuizAnswer());
//                question.answers = list.ToArray();
//                serializedObject.ApplyModifiedProperties();
//            }
//
//            EditorGUILayout.EndHorizontal();
//
//            if (question.answers != null)
//            {
//                for (var index = 0; index < question.answers.Length; index++)
//                {
//                    var questionAnswer = question.answers[index];
//
//
//                    EditorGUILayout.BeginHorizontal();
//
//                    questionAnswer.isCorrect = GUILayout.Toggle(questionAnswer.isCorrect, "Is Correct");
//
//                    EditorGUILayout.BeginVertical();
//                    GUILayout.Label("Initial answer");
//                    questionAnswer.unanswerText = GUILayout.TextField(questionAnswer.unanswerText);
//                    GUILayout.Label("After check answer");
//                    questionAnswer.answerText = GUILayout.TextField(questionAnswer.answerText);
//                    EditorGUILayout.EndVertical();
//                    if (questionAnswer != question.answers[0] && GUILayout.Button("↑"))
//                    {
//                        ArrayHelpers.ShiftOneDown(question.answers, index);
//                    }
//
//                    if (questionAnswer != question.answers[question.answers.Length - 1] && GUILayout.Button("↓"))
//                    {
//                        ArrayHelpers.ShiftOneUp(question.answers, index);
//                    }
//
//                    EditorGUILayout.EndHorizontal();
//                }
//
//            }
        }
    }
}