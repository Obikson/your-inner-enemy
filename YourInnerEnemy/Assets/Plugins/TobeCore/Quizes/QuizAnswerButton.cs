﻿using Plugins.TobeCore.UI;
using TMPro;
using UnityEngine;

namespace Plugins.TobeCore.Quizes
{
    public class QuizAnswerButton : MonoBehaviour
    {
        public TextMeshProUGUI answerSymbol;
        public TextMeshProUGUI text;
    }
}