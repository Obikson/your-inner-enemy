﻿using System.Linq;
using UnityEngine;

namespace Plugins.TobeCore.Quizes
{
    [CreateAssetMenu(fileName = "QuizQuestion", menuName = "Quizes/Question")]
    public class QuizQuestion : ScriptableObject
    {
        public string text;

        public QuizAnswer[] answers;

        public int GetCorrectAnswersCount()
        {
            return answers.Count(x => x.isCorrect);
        }
    }
}