﻿using System;
using Plugins.TobeCore.UI;
using UnityEngine;

namespace Plugins.TobeCore.Quizes
{
    [Serializable]
    public  class QuizAnswer : IFormData
    {
        public bool isCorrect;
        
        /// <summary>
        /// Chci zobrazit nez odpovi
        /// </summary>
        public string unanswerText;
        
        /// <summary>
        /// Zobrazim az odpovi
        /// </summary>
        public string answerText;

        public string Name { get; set; }
        public string Value { get; set; }
    }
}